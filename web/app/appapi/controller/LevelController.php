<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 会员等级
 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class LevelController extends HomebaseController {
	
	
	function level(){
		$list=Db::name("level")->order("level asc")->select()->toArray();
		foreach($list as $k=>$v){
			$list[$k]['level_up']=number_format($v['level_up']);
			$list[$k]['thumb']=get_upload_path($v['thumb']);
		}
		$this->assign("list",$list);
		return $this->fetch();
	}

	function level_a(){
		$list=Db::name("level_anchor")->order("level asc")->select()->toArray();
		foreach($list as $k=>$v){
			$list[$k]['level_up']=number_format($v['level_up']);
			$list[$k]['thumb']=get_upload_path($v['thumb']);
		}
		$this->assign("list",$list);
		return $this->fetch();
	}
}