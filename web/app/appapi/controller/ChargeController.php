<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 充值 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class ChargeController extends HomebaseController{

	public function lists() {


        $level=DB::name('level')
                ->order("level asc")
                ->select();
        foreach($level as $k=>$v){
            $v['thumb']=get_upload_path($v['thumb']);
            $level[$k]=$v;
        }


        return $level;
	}	
    

	

}