<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 提现记录 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class CashController extends HomebaseController{

    var $status=array("0"=>"处理中","1"=>"已通过","2"=>"已拒绝");
    
    public function index() {        
        $data = $this->request->param();
        $uid=(int)checkNull($data['uid']);
        $token=checkNull($data['token']);
        
        $checkToken=checkToken($uid,$token);

		if($checkToken==700){
			$reason='您的登陆状态失效，请重新登陆！';
			$this->assign('reason', $reason);
			return $this->fetch(':error');
		}
        
        $map[]=['uid','=',$uid];
        
        $list = Db::name('cash_record')
            ->where($map)
            ->order("id desc")
            ->paginate(50);
        
        $list->each(function($v,$k){
            
           $v['status_name']=$this->status[$v['status']];
           $v['addtime']=date('Y.m.d',$v['addtime']);
           
           return $v; 
        });
        
        $this->assign('list', $list);
        $this->assign('uid', $uid);
        $this->assign('token', $token);

        return $this->fetch();
	}
    
    public function more() {
        $result=array(
			'list'=>[],
			'nums'=>0,
			'isscroll'=>0,
		);
        
        $data = $this->request->param();
        $uid=(int)checkNull($data['uid']);
        $token=checkNull($data['token']);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			return $result;
		}
        
        $map[]=['uid','=',$uid];
        $pnums=50;
        $list = Db::name('cash_record')
            ->where($map)
            ->order("id desc")
            ->paginate($pnums);
        
        $list->each(function($v,$k){
            
           $v['status_name']=$this->status[$v['status']];
           $v['addtime']=date('Y.m.d',$v['addtime']);
           return $v; 
        });
        
        $nums=count($list);
		if($nums<$pnums){
			$isscroll=0;
		}else{
			$isscroll=1;
		}
        
        $result=array(
			'list'=>$list,
			'nums'=>$nums,
			'isscroll'=>$isscroll,
		);

        return $result;

	}

}