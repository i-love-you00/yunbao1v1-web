<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 支出、收益明细 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class RecordController extends HomebaseController{
    
    var $type=array("0"=>"支出","1"=>"收入");
    var $action=array("1"=>"赠送礼物","2"=>"视频通话","3"=>"语音通话","4"=>"视频付费","5"=>"照片付费","6"=>"购买VIP","7"=>"购买私信","8"=>"注册奖励");
    
    public function expend() {        
        $data = $this->request->param();
        $uid=(int)checkNull($data['uid']);
        $token=checkNull($data['token']);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$reason='您的登陆状态失效，请重新登陆！';
			$this->assign('reason', $reason);
			return $this->fetch(':error');
		}
        
        $map[]=['uid','=',$uid];
        
        $list = Db::name('user_coinrecord')
            ->where($map)
            ->order("id desc")
            ->paginate(50);
        
        $list->each(function($v,$k){
           $userinfo= getUserInfo($v['touid']);
		   if($v['action']==6 || $v['action']==7){
			$userinfo['user_nickname']="平台";
		   }
           $v['user_nickname']=$userinfo['user_nickname'];
           $v['actionname']=$this->action[$v['action']];
           $v['addtime']=date('m/d H:i',$v['addtime']);
           return $v; 
        });
        
        $this->assign('list', $list);
        $this->assign('uid', $uid);
        $this->assign('token', $token);

        return $this->fetch();
	}
    
    public function expendmore() {
        $result=array(
			'list'=>[],
			'nums'=>0,
			'isscroll'=>0,
		);
        
        $data = $this->request->param();
        $uid=(int)checkNull($data['uid']);
        $token=checkNull($data['token']);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			return $result;
		}
        
        $map[]=['uid','=',$uid];
        $pnums=50;
        $list = Db::name('user_coinrecord')
            ->where($map)
            ->order("id desc")
            ->paginate($pnums);
        
        $list->each(function($v,$k){
           $userinfo= getUserInfo($v['touid']);
		   if($v['action']==6 || $v['action']==7){
			$userinfo['user_nickname']="平台";
		   }
           $v['user_nickname']=$userinfo['user_nickname'];
           $v['actionname']=$this->action[$v['action']];
           $v['addtime']=date('m/d H:i',$v['addtime']);
           return $v; 
        });
        
        $nums=count($list);
		if($nums<$pnums){
			$isscroll=0;
		}else{
			$isscroll=1;
		}
        
        $result=array(
			'list'=>$list,
			'nums'=>$nums,
			'isscroll'=>$isscroll,
		);

        return $result;

	}	
    
    public function income() {        
        $data = $this->request->param();
        $uid=(int)checkNull($data['uid']);
        $token=checkNull($data['token']);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$reason='您的登陆状态失效，请重新登陆！';
			$this->assign('reason', $reason);
			return $this->fetch(':error');
		}
        
        $map[]=['uid','<>',$uid];
        $map[]=['touid','=',$uid];
        $map[]=['isdeduct','=','0'];
        
        $list = Db::name('user_coinrecord')
            ->where($map)
            ->order("id desc")
            ->paginate(50);
        
        $list->each(function($v,$k){
           $userinfo= getUserInfo($v['uid']);
           $v['user_nickname']=$userinfo['user_nickname'];
           $v['actionname']=$this->action[$v['action']];
           $v['addtime']=date('m/d H:i',$v['addtime']);
           return $v; 
        });
        
        $this->assign('list', $list);
        $this->assign('uid', $uid);
        $this->assign('token', $token);

        return $this->fetch();
	}	

    public function incomemore() {   

        $result=array(
			'list'=>[],
			'nums'=>0,
			'isscroll'=>0,
		);
        
        $data = $this->request->param();
        $uid=(int)checkNull($data['uid']);
        $token=checkNull($data['token']);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			return $result;
		}
        
        $map[]=['uid','<>',$uid];
        $map[]=['touid','=',$uid];
        $map[]=['isdeduct','=','0'];
        
        $pnums=50;
        $list = Db::name('user_coinrecord')
            ->where($map)
            ->order("id desc")
            ->paginate($pnums);
        
        $list->each(function($v,$k){
           $userinfo= getUserInfo($v['uid']);
           $v['user_nickname']=$userinfo['user_nickname'];
           $v['actionname']=$this->action[$v['action']];
           $v['addtime']=date('m/d H:i',$v['addtime']);
           return $v; 
        });
        

        
        $nums=count($list);
		if($nums<$pnums){
			$isscroll=0;
		}else{
			$isscroll=1;
		}
        
        $result=array(
			'list'=>$list,
			'nums'=>$nums,
			'isscroll'=>$isscroll,
		);

        return $result;
	}	
	

}