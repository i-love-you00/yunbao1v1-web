<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use think\Db;
use think\db\Query;
use app\portal\service\PostService;

class PageController extends HomeBaseController
{
    /**
     * 页面管理
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function indexBF(){

        $postService = new PostService();
        $pageId      = $this->request->param('id', 0, 'intval');
        $page        = $postService->publishedPage($pageId);

        if (empty($page)) {
            abort(404, ' 页面不存在!');
        }

        $this->assign('page', $page);

        $more = $page['more'];

        $tplName = empty($more['template']) ? 'page' : $more['template'];

        return $this->fetch("/$tplName");
    }

    public function index(){

        $id=$this->request->param('id');

        if(!$id){
            $this->assign("reason",'参数错误');
            $this->display(':error');
            exit;
        }

        $info=Db::name("portal_post")->field("id,post_title,post_content")->where("id={$id} and post_type=2")->find();

        if(!$info){
            $this->assign("reason",'参数错误');
            $this->display(':error');
            exit;
        }

        $info['post_content']=htmlspecialchars_decode($info['post_content']);

        $this->assign("info",$info);

        return $this->fetch();
    }

}
