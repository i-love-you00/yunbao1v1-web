<?php

/* 充值记录 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ChargeController extends AdminBaseController
{
    var $status=array("0"=>"未支付","1"=>"已完成");
    var $type=array("1"=>"支付宝","2"=>"微信","3"=>"苹果支付","4"=>"Paypal支付","5"=>"支付宝H5支付");
    var $ambient=array(
            "1"=>array(
                '0'=>'App',
                '1'=>'PC',
            ),
            "2"=>array(
                '0'=>'App',
                '1'=>'公众号',
                '2'=>'PC',
            ),
            "3"=>array(
                '0'=>'沙盒',
                '1'=>'生产',
            ),
            "4"=>array(
                '0'=>'沙盒',
                '1'=>'生产',
            ),
			"5"=>array(
                '0'=>'App',
                '1'=>'PC',
            ),
        );
        
    public function index()
    {
        
        $data = $this->request->param();
        $map=[];
        if($data['status']!=''){
            $map[]=['status','=',$data['status']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid','=',intval($data['uid'])];
        }
        
        if($data['keyword']!=''){
            $map[]=['orderno|trade_no','like',"%".$data['keyword']."%"];
        }

        
        $list = Db::name('charge_user')
            ->where($map)
            ->order("id desc")
            ->paginate(20);
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           return $v; 
        });
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
        
        $total = Db::name('charge_user')
            ->where($map)
            ->sum("money");
        if(!$total){
            $total=0;
        }
            
        $this->assign('total', $total);
        $this->assign('list', $list);
        $this->assign('status', $this->status);
        $this->assign('type', $this->type);
        $this->assign('ambient', $this->ambient);

        return $this->fetch();
    }


    public function setPay()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        if(!$id){
            $this->error("数据传入失败！");
        }
        
        $result=DB::name("charge_user")->where("id={$id}")->find();				
        if($result){
            if($result['status']==1){
                $this->error("该订单已支付成功");
            }
            /* 更新会员虚拟币 */
            $coin=$result['coin']+$result['coin_give'];
            
            DB::name("user")->where("id='{$result['touid']}'")->setInc("coin",$coin);
            /* 更新 订单状态 */
            DB::name("charge_user")->where("id='{$result['id']}'")->update(array("status"=>1));
                

            $this->success('操作成功');
         }else{
            $this->error('数据传入失败！');
         }	
             
    }
    
    public function export()
    {
        
        $data = $this->request->param();
        $map=[];
        if($data['status']!=''){
            $map[]=['status','=',$data['status']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid','=',intval($data['uid'])];
        }
        
        if($data['keyword']!=''){
            $map[]=['orderno|trade_no','like',"%".$data['keyword']."%"];
        }

        
        $list = Db::name('charge_user')
            ->where($map)
            ->order("id desc")
            ->select();
            
        $list->each(function($v,$k){
           $userinfo= getUserInfo($v['uid']);

            $v['user_nickname']= $userinfo['user_nickname'].'('.$v['uid'].')';
            $v['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
			$v['ambient']=$this->ambient[$v['type']][$v['ambient']];
            $v['type']=$this->type[$v['type']];
            $v['status']=$this->status[$v['status']];
            
            
           return $v; 
        });
		
		

        
        $xlsName  = "充值";

        $cellName = array('A','B','C','D','E','F','G','H','I','J','K');
        $xlsCell  = array(
            array('id','ID'),
            array('user_nickname','用户'),
            array('money','人民币金额'),
            array('coin','兑换点数'),
            array('coin_give','赠送点数'),
            array('orderno','商户订单号'),
            array('type','支付类型'),
            array('ambient','支付环境'),
            array('trade_no','第三方支付订单号'),
            array('status','订单状态'),
            array('addtime','提交时间')
        );
        exportExcel($xlsName,$xlsCell,$list,$cellName);
    }


}