<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 等级 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class LevelController extends AdminBaseController
{

    public function index()
    {
        
        $list = Db::name('level')
            ->order("level asc")
            ->paginate(20);
        $list->each(function($v, $k){
            $v['thumb']=get_upload_path($v['thumb']);
            return $v;
        });
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);

        return $this->fetch();
    }


    public function add()
    {
        return $this->fetch();
    }

    public function addPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();
            
            if($data['level'] == ''){
                $this->error('等级不能为空');
            }else{
                $check = Db::name('level')->where("level='{$data['level']}'")->find();
                if($check){
                    $this->error('等级不能重复');
                }
            }
            
            $level_up=$data['level'];
            $thumb=$data['thumb'];
            
            if($level_up==''){
                $this->error('请填写经验上限');
            }

            if($thumb==''){
                $this->error('请上传图标');
            }
            
            
            $id = DB::name('level')->insertGetId($_POST);
            if(!$id){
                $this->error("添加失败！");
            }
            $this->resetcache();
            $this->success("添加成功！");
        }
    }

    public function edit()
    {
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('level')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function editPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();

            if($data['level'] == ''){
                $this->error('等级不能为空');
            }else{
                $check = Db::name('level')->where("level='{$data['level']}' and id !='{$data['id']}'")->find();
                if($check){
                    $this->error('等级不能重复');
                }
            }
            
            $level_up=$data['level'];
            $thumb=$data['thumb'];
            
            if($level_up==''){
                $this->error('请填写经验上限');
            }

            if($thumb==''){
                $this->error('请上传图标');
            }
            
            
            $rs = DB::name('level')->update($data);

            if($rs === false){
                $this->error("保存失败！");
            }
            $this->resetcache();
            $this->success("保存成功！");
        }
    }


    public function del()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('level')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        $this->resetcache();
        $this->success("删除成功！",url("level/index"));
    }


    protected function resetcache(){
        $key='level';

        $level=DB::name('level')
                ->order("level asc")
                ->select();
        foreach($level as $k=>$v){
            $v['thumb']=get_upload_path($v['thumb']);
            $level[$k]=$v;
        }
        if($level){
            setcaches($key,$level);
        }
    }
}