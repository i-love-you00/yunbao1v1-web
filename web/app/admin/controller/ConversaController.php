<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 通话记录 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ConversaController extends AdminBaseController
{
    var $type=array("1"=>"视频通话","2"=>"语音通话");
    var $status=array("0"=>"等待接听","1"=>"通话中","2"=>"通话结束");
        
    public function index()
    {
        
        $data = $this->request->param();
        $map=[];
        if($data['type']!=''){
            $map[]=['type','=',$data['type']];
        }
        
        if($data['status']!=''){
            $map[]=['status','=',$data['status']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['starttime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['starttime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid|liveuid','=',intval($data['uid'])];
        }
        
        $list = Db::name('conversa_log')
            ->where($map)
            ->order("id desc")
            ->paginate(20);
        
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           $v['liveinfo']= getUserInfo($v['liveuid']);
           $length=0;
           if($v['status']==2){
               if($v['starttime'] && $v['endtime']){
                    $cha=$v['endtime']-$v['starttime'];
                    $length=getLength($cha);
               }
           }
           $v['length']=$length;
           
           return $v; 
        });
        
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        $this->assign('type', $this->type);
        $this->assign('status', $this->status);

        return $this->fetch();
    }

}