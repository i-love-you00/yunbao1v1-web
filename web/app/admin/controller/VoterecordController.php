<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 云票记录
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class VoterecordController extends AdminbaseController {
    
    protected function getTypes($k=''){
        $type=array(
            '0'=>'支出',
            '1'=>'收入',
        );
        if($k===''){
            return $type;
        }
        
        return isset($type[$k]) ? $type[$k]: '';
    }
    
    protected function getAction($k=''){
        $action=array(
            '1'=>'注册邀请奖励',
            '3'=>'收礼物',
            '4'=>'视频通话',
            '5'=>'语音通话',
            '6'=>'视频收费',
            '8'=>'通话不足10秒主播退回通话收益给平台',
        );
        if($k===''){
            return $action;
        }
        
        return isset($action[$k]) ? $action[$k]: '未知';
    }


    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $type=isset($data['type']) ? $data['type']: '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $action=isset($data['action']) ? $data['action']: '';
        if($action!=''){
            $map[]=['action','=',$action];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
           
            $map[]=['uid','=',$uid];
            
        }
        
        $fromid=isset($data['fromid']) ? $data['fromid']: '';
        if($fromid!=''){
            $map[]=['fromid','=',$fromid];
        }
        
        $lists = Db::name("user_voterecord")
            ->where($map)
			->order("id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['touserinfo']=getUserInfo($v['fromid']);
            
            $action=$v['action'];
            if($action=='3'){
                $info=Db::name("gift")->field("name")->where("id='{$v['actionid']}'")->find();
                $giftinfo['giftname']=$info['name'];
            }else if($action=='6'){
                $info=Db::name("video")->field("title")->where("id='{$v['actionid']}'")->find();
                if($info['title']==''){
                    $info['title']=$this->getAction($action);
                }
                $giftinfo['giftname']=$info['title'];
            }else{
                $giftinfo['giftname']=$this->getAction($action);
                
            }
            $v['giftinfo']= $giftinfo;
                
            return $v;           
        });
    	
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
        $this->assign('action', $this->getAction());
        $this->assign('type', $this->getTypes());
        
    	return $this->fetch();
    
    }
		
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('user_voterecord')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
                    
        $this->success("删除成功！");
        							  			
    }    	
}
