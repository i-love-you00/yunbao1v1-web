<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\Menu;

class MainController extends AdminBaseController
{

    /**
     *  后台欢迎页
     */
    public function index()
    {
        $nowtime=time();
        //当天0点
        $today=date("Ymd",$nowtime);
        $today_start=strtotime($today);
        //当天 23:59:59
        $today_end=strtotime("{$today} + 1 day");
        
        $yesterday_start=$today_start - 60*60*24;
 
        /* 通话统计 */
        /* 语音时间 */
        $live_voice_a=Db::name('conversa_log')->field("sum(endtime) as endtotal,sum(starttime) as starttotal")->where("status=2 and type=2 and endtime!=0 and starttime!=0 ")->find();
        $live_voice=0;
        if($live_voice_a){
            $live_voice=$live_voice_a['endtotal']-$live_voice_a['starttotal'];
        }
        
        $live_voice_t_a=Db::name('conversa_log')->field("sum(endtime) as endtotal,sum(starttime) as starttotal")->where("status=2 and type=2 and endtime!=0 and starttime!=0 and starttime >= {$today_start} and starttime < {$today_end}")->find();
        $live_voice_t=0;
        if($live_voice_t_a){
            $live_voice_t=$live_voice_t_a['endtotal']-$live_voice_t_a['starttotal'];
        }
        
        $live_voice_y_a=Db::name('conversa_log')->field("sum(endtime) as endtotal,sum(starttime) as starttotal")->where("status=2 and type=2 and endtime!=0 and starttime!=0 and starttime >= {$yesterday_start} and starttime < {$today_start}")->find();
        $live_voice_y=0;
        if($live_voice_y_a){
            $live_voice_y=$live_voice_y_a['endtotal']-$live_voice_y_a['starttotal'];
        }
        
        $live_voice_rate='0%';
        $live_voice_rate_c='';
        if($live_voice_t==0 && $live_voice_y==0){
            
        }else if($live_voice_t==0){
            $live_voice_rate=$live_voice_y.'%';
            $live_voice_rate_c='down';
        }else if($live_voice_y==0){
            $live_voice_rate=$live_voice_t.'%';
            $live_voice_rate_c='up';
        }else{
            $rate=floor(($live_voice_t - $live_voice_y)/$live_voice_y*100);
            if($rate>0){
                $live_voice_rate_c='up';
            }else if($rate<0){
                $live_voice_rate_c='down';
            }
            $live_voice_rate=abs($rate).'%';
        }
        $this->assign('live_voice', floor($live_voice/60));
        $this->assign('live_voice_t', floor($live_voice_t/60));
        $this->assign('live_voice_y', floor($live_voice_y/60));
        $this->assign('live_voice_rate', $live_voice_rate);
        $this->assign('live_voice_rate_c', $live_voice_rate_c);
        
        /* 视频时间 */
        $live_video_a=Db::name('conversa_log')->field("sum(endtime) as endtotal,sum(starttime) as starttotal")->where("status=2 and type=1 and endtime!=0 and starttime!=0 ")->find();
        $live_video=0;
        if($live_video_a){
            $live_video=$live_video_a['endtotal']-$live_video_a['starttotal'];
        }
        
        $live_video_t_a=Db::name('conversa_log')->field("sum(endtime) as endtotal,sum(starttime) as starttotal")->where("status=2 and type=1 and endtime!=0 and starttime!=0 and starttime >= {$today_start} and starttime < {$today_end}")->find();
        $live_video_t=0;
        if($live_video_t_a){
            $live_video_t=$live_video_t_a['endtotal']-$live_video_t_a['starttotal'];
        }
        
        $live_video_y_a=Db::name('conversa_log')->field("sum(endtime) as endtotal,sum(starttime) as starttotal")->where("status=2 and type=1 and endtime!=0 and starttime!=0 and starttime >= {$yesterday_start} and starttime < {$today_start}")->find();
        $live_video_y=0;
        if($live_video_y_a){
            $live_video_y=$live_video_y_a['endtotal']-$live_video_y_a['starttotal'];
        }
        
        $live_video_rate='0%';
        $live_video_rate_c='';
        if($live_video_t==0 && $live_video_y==0){
            
        }else if($live_video_t==0){
            $live_video_rate=$live_video_y.'%';
            $live_video_rate_c='down';
        }else if($live_video_y==0){
            $live_video_rate=$live_video_t.'%';
            $live_video_rate_c='up';
        }else{
            $rate=floor(($live_video_t - $live_video_y)/$live_video_y*100);
            if($rate>0){
                $live_video_rate_c='up';
            }else if($rate<0){
                $live_video_rate_c='down';
            }
            $live_video_rate=abs($rate).'%';
        }
        $this->assign('live_video',  floor($live_video/60));
        $this->assign('live_video_t',  floor($live_video_t/60));
        $this->assign('live_video_y',  floor($live_video_y/60));
        $this->assign('live_video_rate', $live_video_rate);
        $this->assign('live_video_rate_c', $live_video_rate_c);
        
        /* 总通话 */
        $lives=$live_voice + $live_video;
        
        $lives_t=$live_voice_t + $live_video_t;
        
        $lives_y=$live_voice_y + $live_video_y;
        
        $lives_rate='0%';
        $lives_rate_c='';
        if($lives_t==0 && $lives_y==0){
            
        }else if($lives_t==0){
            $lives_rate=$lives_y.'%';
            $lives_rate_c='down';
        }else if($lives_y==0){
            $lives_rate=$lives_t.'%';
            $lives_rate_c='up';
        }else{
            $rate=floor(($lives_t - $lives_y)/$lives_y*100);
            if($rate>0){
                $lives_rate_c='up';
            }else if($rate<0){
                $lives_rate_c='down';
            }
            $lives_rate=abs($rate).'%';
        }
        $this->assign('lives', floor($lives/60));
        $this->assign('lives_t', floor($lives_t/60));
        $this->assign('lives_y', floor($lives_y/60));
        $this->assign('lives_rate', $lives_rate);
        $this->assign('lives_rate_c', $lives_rate_c);
        

        return $this->fetch();
    }


}
