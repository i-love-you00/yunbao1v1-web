<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 主播认证 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class AuthorauthController extends AdminBaseController
{
    var $status=array("0"=>"审核中","1"=>"已通过",'2'=>'已拒绝');

    public function index(){
        
        $data = $this->request->param();
        $map=[];
        if($data['status']!=''){
            $map[]=['status','=',$data['status']];
        }

        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        if($data['uid']!=''){
            $map[]=['uid','=',intval($data['uid'])];
        }
 
        $list = Db::name('author_auth')
            ->where($map)
            ->order("addtime desc")
            ->paginate(20);
        
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           
           $v['thumb']=get_upload_path($v['thumb']);
            $list1=explode(',',$v['backwall']);
            
            foreach($list1 as $k1=>$v1){
                $v1=get_upload_path($v1);
                $list1[$k1]=$v1;
            }
            
            $v['backwall_list']=$list1;

            $v['video']=get_upload_path($v['video']);
            $v['video_thumb']=get_upload_path($v['video_thumb']);

           return $v; 
        });

        
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        $this->assign('status', $this->status);

        return $this->fetch();
    }


    public function setstatus(){

        $uid = $this->request->param('uid', 0, 'intval');
        if(!$uid){
            $this->error("数据传入失败！");
        }
        $status = $this->request->param('status', 0, 'intval');
        $reason = $this->request->param('reason');
        
        $result=DB::name("author_auth")->where("uid={$uid}")->find();
        if(!$result){
            $this->error("数据传入失败！");
        }
        
        if($result['status']!=0 && $result['status'] == $status){
            $this->error("操作失败,请刷新重试");
        }

        $user_auth=Db::name("user_auth")->where("uid={$uid}")->find();
        if(!$user_auth || $user_auth['status']!=1){
            $this->error("请先审核该用户实名认证信息");
        }
        
        $nowtime=time();
        
        $rs=DB::name("author_auth")
            ->where("uid={$uid}")
            ->update(
                [
                    'status'=>$status,
                    'reason'=>$reason,
                    'uptime'=>$nowtime
                ]
            );

        if(!$rs){
            $this->error("操作失败");
        }
        
        
        if($status==1){
            // 更新用户信息
            $updata=[
                'isauthor_auth'=>'1',
            ];

            $fee_video=DB::name("fee_video")->order('coin asc')->value('coin');

            if($fee_video){
                $updata['isvideo']='1';
                $updata['video_value']=$fee_video;
            }
            
            DB::name("user")->where("id={$uid}")->update($updata);

            
            $this->resetcache($uid);
            
            /* 更新背景墙 */
            $isexist=DB::name("backwall")->where("uid={$uid}")->find();
            if(!$isexist){
                $thumb=$result['thumb'];
                $photos=explode(',',$result['backwall']);
                $data=[
                    'uid'=>$uid,
                    'thumb'=>$thumb,
                    'href'=>'',
                    'type'=>-1,
                ];
                DB::name("backwall")->insert($data);
                
                foreach($photos as $k1=>$v1){
                    if($v1){
                        $data=[
                            'uid'=>$uid,
                            'thumb'=>$v1,
                            'href'=>'',
                            'type'=>0,
                        ];
                        DB::name("backwall")->insert($data);                   
                    }
                }
            }
        
        }

        if($status==2){
            Db::name("user")->where("id={$uid}")
                ->update(
                    [
                        'isauthor_auth'=>0,
                        'isvoice'=>0,
                        'voice_value'=>0,
                        'isvideo'=>0,
                        'video_value'=>0

                    ]
                );
        }

        /* IM */
        $content='您的主播认证审核未通过，失败原因：'.$reason;
        if($status==1){
            $content='恭喜！您的主播认证审核通过了';
        }
        $this->sendIm($uid,$content);
        
        $this->success("操作成功");        
    }


    
    protected function sendIm($uid,$content){

            
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMTextElem',       //自定义类型
            'MsgContent' => array(
                //'Data' => json_encode($ext),
                'Desc' => '',
                'Text' => $content,
                //  'Ext' => $ext,
                //  'Sound' => '',
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=0;
        $receiver=(string)$uid;
        $api=getTxRestApi();
        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content,1);
 
        return 1;
    }

    protected function resetcache($uid){
        $key='author_auth_'.$uid;
        delcache($key);

        $info=DB::name('author_auth')
                ->field('*')
                ->where("uid={$uid}")
                ->find();
        if($info){
            setcaches($key,$info);
        }
    }


    public function view(){

        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('author_auth')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['href']=get_upload_path($data['video']);
        
        $this->assign('data', $data);
        return $this->fetch();
    }

}