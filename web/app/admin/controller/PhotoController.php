<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


/* 相册管理 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class PhotoController extends AdminBaseController
{
    var $type_private=[
        '0'=>'否',
        '1'=>'是',
    ];
    
    var $status=[
        '0'=>'待审核',
        '1'=>'审核通过',
    ];
    public function index()
    {
        $data = $this->request->param();
        $map=[];
        if($data['status']!=''){
            $map[]=['status','=',$data['status']];
        }

        if($data['uid']!=''){
            $map[]=['uid','=',intval($data['uid'])];
        }
        
        $list = Db::name('photo')
            ->where($map)
            ->order("id desc")
            ->paginate(20);
        $list->each(function($v, $k){
            $userinfo=getUserInfo($v['uid']);
            $v['userinfo']=$userinfo;
            $v['thumb']=get_upload_path($v['thumb']);
            return $v;
        });
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        
        $this->assign('type_private', $this->type_private);
        $this->assign('status', $this->status);

        return $this->fetch();
    }


    public function add()
    {
        return $this->fetch();
    }

    public function addPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();
            
            $uid=$data['uid'];
            
            
            if($uid < 0){
                $this->error('请填写正确的用户ID');
            }else{
                $check = Db::name('user')->where("id='{$uid}'")->find();
                if(!$check){
                    $this->error('该用户不存在');
                }
            }
            
            
            $isprivate=$data['isprivate'];
            $thumb=$data['thumb'];
            $coin=$data['coin'];
            
            if($isprivate==1){
                if($coin<=0){
                    $this->error('请输入正确价格');
                }
                
            }

            if($thumb==''){
                $this->error('请上传图片');
            }
                
            $data['addtime']=time();
            $data['uptime']=time();
            $data['status']='1';

            
            $id = DB::name('photo')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            $this->resetcache();
            $this->success("添加成功！");
        }
    }

    public function edit()
    {
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('photo')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['userinfo']= getUserInfo($data['uid']);
        
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function editPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();

            $isprivate=$data['isprivate'];
            $thumb=$data['thumb'];
            $coin=$data['coin'];
            
            if($isprivate==1){
                if($coin<=0){
                    $this->error('请输入正确价格');
                }
            }else{
               $data['coin']=time(); 
            }

            if($thumb==''){
                $this->error('请上传图片');
            }
            
            $data['uptime']=time();
            
            $rs = DB::name('photo')->update($data);

            if($rs === false){
                $this->error("保存失败！");
            }
            $this->resetcache();
            $this->success("保存成功！");
        }
    }


    public function del()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('photo')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        $this->resetcache();
        $this->success("删除成功！",url("photo/index"));
    }
    
    public function setstatus()
    {
        $id = $this->request->param('id', 0, 'intval');
        if(!$id){
            $this->error("数据传入失败！");
        }
        $status = $this->request->param('status', 0, 'intval');
        $reason = $this->request->param('reason');
        
        $result=DB::name("photo")->where("id={$id}")->find();
        if(!$result){
            $this->error("数据传入失败！");
        }
        
        if($result['status']!=0 && $result['status'] == $status){
            $this->error("操作失败");
        }
        
        $nowtime=time();        
        if($status==2){
            DB::name("photo")->where("id={$id}")->delete();
        }else{
            $rs=DB::name("photo")->where("id={$id}")->update(['status'=>$status,'reason'=>$reason,'uptime'=>$nowtime]);
            if(!$rs){
                $this->error("操作失败");
            }
        }
        
        
        /* IM */
        $content='您上传的照片审核未通过,已被删除，失败原因：'.$reason;
        if($status==1){
            $content='恭喜！您上传的照片审核通过了';
        }
        $this->sendIm($result['uid'],$content);
        
        $this->success("操作成功");        
    }

    protected function sendIm($uid,$content){
        
        /* IM */
            
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMTextElem',       //自定义类型
            'MsgContent' => array(
                'Desc' => '',
                'Text' => $content,
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=0;
        $receiver=(string)$uid;
        $api=getTxRestApi();
        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content,1);
     
        return 1;
    }
    
    protected function resetcache(){
        
    }
}