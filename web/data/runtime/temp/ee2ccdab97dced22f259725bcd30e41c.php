<?php /*a:2:{s:87:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/admin/dynamic/passindex.html";i:1646881836;s:77:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/public/header.html";i:1646881836;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a >审核通过列表</a></li>
			<!-- <li><a href="<?php echo url('Dynamic/add'); ?>">添加</a></li> -->
		</ul>
        
        <form class="well form-inline margin-top-20" method="post" action="<?php echo url('Dynamic/passindex'); ?>">
			排序：
			<select class="form-control" name="ordertype" style="width: 150px;">
				<option value="">默认</option>
				<option value="1" <?php if(input('request.ordertype') == '1'): ?>selected<?php endif; ?> >评论数排序</option>
				<option value="2" <?php if(input('request.ordertype') == '2'): ?>selected<?php endif; ?> >点赞数排序</option>
			</select>
			动态类型：
            <select class="form-control" name="dttype" style="width: 150px;">
                <option value=''>全部</option>
                <?php if(is_array($dttype) || $dttype instanceof \think\Collection || $dttype instanceof \think\Paginator): $i = 0; $__LIST__ = $dttype;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo $key; ?>" <?php if(input('request.dttype') != '' && input('request.dttype') == $key): ?>selected<?php endif; ?>><?php echo $v; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
            关键字：
            <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>" placeholder="请输入用户ID">
            <input class="form-control" type="text" name="dynamicid" style="width: 200px;" value="<?php echo input('request.dynamicid'); ?>" placeholder="请输入动态ID">
            <input class="form-control" type="text" name="keyword1" style="width: 200px;" value="<?php echo input('request.keyword1'); ?>" placeholder="请输入动态标题">
		
            <input type="submit" class="btn btn-primary" value="搜索"/>
            <a class="btn btn-danger" href="<?php echo url('Dynamic/passindex'); ?>">清空</a>
        </form>
        
		<form method="post" class="js-ajax-form" >
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>用户(ID)</th>
						<th>标题</th>
						<th >封面</th>
					<!-- 	<th  style="max-width:100px;">视频地址</th>
						<th  style="max-width:100px;">语音地址</th> -->
						<th>点赞数量</th>
						<th>评论数量</th>
						<th>动态类型</th>
                        <th>发布时间</th>
                        <th>状态</th>
						<th align="center"><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$type=array("0"=>"纯文字","1"=>"文字+图片","2"=>"文字+视频","3"=>"文字+语音");
				 if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
					<tr>
						<td><?php echo $vo['id']; ?></td>
                        <td><?php echo $vo['userinfo']['user_nickname']; ?> (<?php echo $vo['uid']; ?>)</td>
						<td><?php echo $vo['title']; ?></td>
                        <td>
						<?php if($vo['type'] == '1'): ?>
							<img src="<?php echo $vo['thumbs']; ?>" class="imgtip" style="max-width:100px;max-height:100px;">
						<?php elseif($vo['type'] == '2'): ?>
							<img src="<?php echo $vo['video_thumb']; ?>" class="imgtip" style="max-width:100px;max-height:100px;">
						<?php else: ?>
						<?php endif; ?>
						</td>
                    <!--     <td  style="max-width:100px;"><?php echo $vo['href']; ?></td>
                        <td  style="max-width:100px;"><?php echo $vo['voice']; ?></td> -->
                        <td><?php echo $vo['likes']; ?></td>
                        <td><?php echo $vo['comments']; ?></td>
                        <td><?php echo $type[$vo['type']]; ?></td>
                        <td><?php echo date('Y-m-d H:i',$vo['addtime']); ?></td>
                        <td><?php echo $status[$vo['status']]; ?></td>
						<td>
							<?php if($vo['type'] == 2): ?>
								<a class="btn btn-xs btn-info view" data-id="<?php echo $vo['id']; ?>" >观看视频</a>
							<?php elseif($vo['type'] == 1): ?>
								<a class="btn btn-xs btn-info" href='<?php echo url("Dynamic/look",array("id"=>$vo["id"])); ?>'>查看图片</a>
							<?php elseif($vo['type'] == 3): ?>
								<a class="btn btn-xs btn-info playyy" data-id="<?php echo $vo['id']; ?>" >播放语音</a>
							<?php else: ?>
							<?php endif; ?>
							<a class="btn btn-xs btn-primary xiajia" data-id="<?php echo $vo['id']; ?>">下架</a>
                            <?php if($vo['status'] == 0): ?>
                                <a class="btn btn-xs btn-success setstatus" data-id="<?php echo $vo['id']; ?>" data-status="1">同意</a>
                                <a class="btn btn-xs btn-danger setstatus" data-id="<?php echo $vo['id']; ?>" data-status="2">拒绝</a>
                            <?php endif; if($vo['status'] == 1): ?>
                                <a class="btn btn-xs btn-danger setstatus" data-id="<?php echo $vo['id']; ?>" data-status="2">拒绝</a>
                            <?php endif; if($vo['status'] == 2): ?>
                                <a class="btn btn-xs btn-success setstatus" data-id="<?php echo $vo['id']; ?>" data-status="1">同意</a>
                            <?php endif; ?>
							<!-- <a class="btn btn-xs btn-success" href='<?php echo url("Dynamic/commentlists",array("id"=>$vo["id"])); ?>'>评论列表</a> -->
							<a class="btn btn-xs btn-success" href="javascript:parent.openIframeLayer('<?php echo url('Dynamic/commentlists',array('id'=>$vo['id'])); ?>','评论列表',{});">评论列表</a>
                            <!-- <a class="btn btn-xs btn-primary" href='<?php echo url("Dynamic/edit",array("id"=>$vo["id"])); ?>'><?php echo lang('EDIT'); ?></a> -->
							<a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('Dynamic/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>

		</form>
	</div>
    <div id="enlarge_images" style="position:fixed;display:none;z-index:2;background:#fff;"></div>
	<script src="/static/js/admin.js"></script>
        <script>
        $(function(){
            var ei=$('#enlarge_images');
            var imgtip=$('.imgtip');
            
            imgtip.mousemove(function(event){
                event = event || window.event;
                
                var html = '<img src="' + this.src + '" style="max-width:500px;max-height:500px;"/>';
                
                ei.html(html);
                var top  = document.body.scrollTop + event.clientY + 10 + "px";
                var left = document.body.scrollLeft + event.clientX + 10 + "px";
            
                var css={
                    'display':'block',
                    'top':top,
                    'left':left,
                }
                ei.css(css);
            })
            
            imgtip.mouseout(function(){
                ei.html('');                
                var css={
                    'display':'none',
                }
                ei.css(css);
            })
            imgtip.click(function(){
                ei.html('');                
                var css={
                    'display':'none',
                }
                ei.css(css);
                window.open( this.src );
            })
            Wind.use('layer');
            $('.setstatus').click(function(){
                var _this=$(this);
                var id=_this.data('id');
                var status=_this.data('status');
                var value=' ';

                layer.prompt({
                    formType: 2,
                    title: '审核意见',
                    value: value,
                    area: ['800px', '100px'] //自定义文本域宽高
                }, function(value, index, elem){
					if(status==2){
						value=value.trim();
						if(value=='' || !value){
							layer.msg("请填写审核意见");
							return !1;
						}
					}
				
                    $.ajax({
                        url:'<?php echo url('Dynamic/setstatus'); ?>',
                        type:'POST',
                        data:{id:id,status:status,reason:value},
                        dataType:'json',
                        success:function(data){
                            var code=data.code;
                            if(code==0){
                                layer.msg(data.msg);
                                return !1;
                            }
                            layer.msg(data.msg,{},function(){
                                layer.closeAll();
                                reloadPage(window);
                            });
                            
                        },
                        error:function(){
                            layer.msg('操作失败，请重试');
                        }
                    });
                    
                });
                
            });
			//下架视频
			 $('.xiajia').click(function(){
                var _this=$(this);
                var id=_this.data('id');
                var value=' ';
                layer.prompt({
                    formType: 2,
                    title: '下架原因',
                    value: value,
                    area: ['800px', '100px'] //自定义文本域宽高
                }, function(value, index, elem){
					value=value.trim();
					if(value=='' || !value){
						layer.msg("请填写审核意见");
						return !1;
					}
				
                    $.ajax({
                        url:'<?php echo url('dynamic/setXiajia'); ?>',
                        type:'POST',
                        data:{id:id,reason:value},
                        dataType:'json',
                        success:function(data){
                            var code=data.code;
                            if(code==0){
                                layer.msg(data.msg);
								layer.closeAll();
                                reloadPage(window);
                                return !1;
                            }
                            layer.msg(data.msg,{},function(){
                                layer.closeAll();
                                reloadPage(window);
                            });
                            
                        },
                        error:function(){
                            layer.msg('操作失败，请重试');
                        }
                    });
                    
                });
                
            });
			//观看视频
            $('.view').click(function(){
                var _this=$(this);
                var id=_this.data('id');

                layer.open({
                    type: 2,
                    title: '观看视频',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['500px', '750px'],
                    content: '/admin/Dynamic/view?id='+id
                }); 
                
            });
			//播放语音
            $('.playyy').click(function(){
                var _this=$(this);
                var id=_this.data('id');
                layer.open({
                    type: 2,
                    title: '播放语音',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['400px', '250px'],
                    content: '/admin/Dynamic/playyy?id='+id
                }); 
                
            })
        })
    </script>    
</body>
</html>