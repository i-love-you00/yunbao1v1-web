<?php /*a:3:{s:66:"/data/wwwroot/git1v1.yyyybbbb.com/themes/default/portal/index.html";i:1646881836;s:65:"/data/wwwroot/git1v1.yyyybbbb.com/themes/default/public/head.html";i:1646881836;s:69:"/data/wwwroot/git1v1.yyyybbbb.com/themes/default/public/function.html";i:1646881836;}*/ ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title><?php echo (isset($site_info['site_name']) && ($site_info['site_name'] !== '')?$site_info['site_name']:''); ?></title>
		<meta name="keywords" content="<?php echo (isset($site_info['site_seo_keywords']) && ($site_info['site_seo_keywords'] !== '')?$site_info['site_seo_keywords']:''); ?>"/>
		<meta name="description" content="<?php echo (isset($site_info['site_seo_description']) && ($site_info['site_seo_description'] !== '')?$site_info['site_seo_description']:''); ?>">

		
<?php 
    /*可以加多个方法哟！*/
 ?>
<meta name="author" content="ThinkCMF">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<!-- Set render engine for 360 browser -->
<meta name="renderer" content="webkit">

<!-- No Baidu Siteapp-->
<meta http-equiv="Cache-Control" content="no-siteapp"/>

<!-- HTML5 shim for IE8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
<link rel="icon" href="/favicon.ico" >
<link rel="shortcut icon" href="/favicon.ico">
<link href="/themes/default/public/assets/simpleboot3/themes/simpleboot3/bootstrap.min.css" rel="stylesheet">
<link href="/themes/default/public/assets/simpleboot3/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
      type="text/css">
<!--[if IE 7]>
<link rel="stylesheet" href="/themes/default/public/assets/simpleboot3/font-awesome/4.4.0/css/font-awesome-ie7.min.css">
<![endif]-->
<link href="/themes/default/public/assets/css/style.css" rel="stylesheet">
<style>
    /*html{filter:progid:DXImageTransform.Microsoft.BasicImage(grayscale=1);-webkit-filter: grayscale(1);}*/
    #backtotop {
        position: fixed;
        bottom: 50px;
        right: 20px;
        display: none;
        cursor: pointer;
        font-size: 50px;
        z-index: 9999;
    }

    #backtotop:hover {
        color: #333
    }

    #main-menu-user li.user {
        display: none
    }
</style>
<script type="text/javascript">
    //全局变量
    var GV = {
        ROOT: "/",
        WEB_ROOT: "/",
        JS_ROOT: "static/js/"
    };
</script>
<script src="/themes/default/public/assets/js/jquery-1.10.2.min.js"></script>
<script src="/themes/default/public/assets/js/jquery-migrate-1.2.1.js"></script>
<script src="/static/js/wind.js"></script>
	
		<link rel="stylesheet" type="text/css" href="/static/index/css/full_index.css" />
		<link rel="stylesheet" type="text/css" href="/static/index/css/jquery.fullPage.css" />
	</head>
	<body>

		<div id="fullpage">

			<!--固定导航-->
			<div class="menu">
				<div class="menu_center">
					<div class="sitename fl"><?php echo (isset($site_info['site_name']) && ($site_info['site_name'] !== '')?$site_info['site_name']:''); ?></div>
					<div class="menu_right fr">
						<ul>
							<li data-menuanchor="page1" class="active">
								<a href="#page1">下载演示</a>
							</Li>
							<li data-menuanchor="page2">
								<a href="#page2">关于我们</a>
							</Li>
						</ul>
					</div>
					<div class="clearboth"></div>
				</div>
			</div>

			<!--page1-->
			<div class="section section1">
				<div class="section_center">
					<div class="section1_left fl">
						<!-- log -->
						<div class="logo_img">
							<img src="/static/index/img/log.png">
						</div>
						<!-- xuanchuanyu -->
						<div class="desc_img">
							<img src="/static/index/img/xuanchuanyu.png">	
						</div>
						<!-- download  -->
						<div class="download">
							<div class="ewm_area ewm fl">
								<div class="ewm_img">
									<?php if($site_info['qr_url'] != ''): ?>
										<img src="<?php echo cmf_get_image_preview_url($site_info['qr_url']); ?>">
									<?php else: ?>
										<img src="/static/index/img/ewm.png">
									<?php endif; ?>
								</div>
							</div>
							<div class="ewm_area fl">
								<div class="ewm_download" style="margin-top: 7%;">
									<img src="/static/index/img/ios.png">
								</div>
								<div class="ewm_download">
									<img src="/static/index/img/adr.png">
								</div>
							</div>
						</div>
					</div>
					<div class="clearboth"></div>
				</div>
			</div>
			<!--page2-->
			<div class="section section2">
				<div class="section2_left fl">
					<div class="company_name">
						<?php echo (isset($site_info['company_name']) && ($site_info['company_name'] !== '')?$site_info['company_name']:''); ?>
					</div>
					<div class="company_desc">
						<?php echo (isset($site_info['company_desc']) && ($site_info['company_desc'] !== '')?$site_info['company_desc']:''); ?>
					</div>
				</div>
				<div class="section2_right fr">
					<img src="/static/index/img/shouji.png">
				</div>
				<div class="clearboth"></div>
				<?php if($site_info['copyright_url'] != ''): ?>
					<div class="copyright">
						<a href="<?php echo $site_info['copyright_url']; ?>" target="_blank"><?php echo (isset($site_info['copyright']) && ($site_info['copyright'] !== '')?$site_info['copyright']:''); ?>
						</a>
					</div>
				<?php else: ?>
					<div class="copyright"><?php echo (isset($site_info['copyright']) && ($site_info['copyright'] !== '')?$site_info['copyright']:''); ?>
					</div>
				<?php endif; ?>
			</div> 
		</div>

		<script type="text/javascript" src="/static/index/js/jquery-1.11.1.js"></script>
		<script type="text/javascript" src="/static/index/js/jquery.fullPage.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {

			$('#fullpage').fullpage({
			
				sectionsColor: ['#fff', '#fff'],
				'navigation': true,
				css3: true, //是否使用 CSS3 transforms 滚动(默认为false)
				anchors:['page1','page2'],  
				menu:".menu",
				scrolllingSpeed:1000,  //滚动速度，单位为毫秒(默认为700)
				
				lockAnchors:false,  //是否锁定锚链接，默认为false。设置weitrue时，锚链接anchors属性也没有效果。
				loopBottom:false,  //滚动到最底部后是否滚回顶部(默认为false)
				loopTop:false, //滚动到最顶部后是否滚底部
				navigationTooltips:["下载演示","关于我们"],//项目导航的 tip

			});

		});
		</script>

	</body>
</html>
