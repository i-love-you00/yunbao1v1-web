<?php /*a:2:{s:87:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/admin/setting/configpri.html";i:1647583475;s:77:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/public/header.html";i:1646881836;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
.cdnhide{
	display:none;
}
</style>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#A" data-toggle="tab">登录设置</a></li>
        <li><a href="#B" data-toggle="tab">支付设置</a></li>
        <li><a href="#C" data-toggle="tab">提现设置</a></li>
        <li><a href="#D" data-toggle="tab">IM设置</a></li>
        <li><a href="#E" data-toggle="tab">直播设置</a></li>
        <li><a href="#F" data-toggle="tab">匹配设置</a></li>
		<li><a href="#I" data-toggle="tab">云存储设置</a></li>
        <li><a href="#K" data-toggle="tab">敏感词设置</a></li>
    </ul>
    <form class="form-horizontal js-ajax-form margin-top-20" role="form" action="<?php echo url('setting/configpriPost'); ?>" method="post">
        <fieldset>
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane active" id="A">
					
						<div class="form-group">
                            <label for="input-reg_reward" class="col-sm-2 control-label">注册奖励</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-reg_reward"
                                       name="options[reg_reward]" value="<?php echo (isset($config['reg_reward']) && ($config['reg_reward'] !== '')?$config['reg_reward']:''); ?>">
                                       <p class="help-block">新用户注册奖励（整数）</p>
                            </div>
                        </div>
					
						<div class="form-group">
                            <label for="input-code_switch" class="col-sm-2 control-label">短信接口平台</label>
                            <div class="col-md-6 col-sm-10" id="cdn">
                                <label class="radio-inline"><input type="radio" value="1" name="options[code_switch]" <?php if(in_array(($config['code_switch']), explode(',',"1"))): ?>checked="checked"<?php endif; ?>>阿里云</label>
                                <label class="radio-inline"><input type="radio" value="2" name="options[code_switch]" <?php if(in_array(($config['code_switch']), explode(',',"2"))): ?>checked="checked"<?php endif; ?>>腾讯云</label>
                               
                            </div>
                        </div>
						
						<div class="form-group">
							<label for="input-sendcode_type" class="col-sm-2 control-label">短信类型</label>
							<div class="col-md-6 col-sm-10">
								<select class="form-control" name="options[sendcode_type]">
									<option value="1">国内</option>
									<option value="2" <?php if($config['sendcode_type'] == '2'): ?>selected<?php endif; ?>>国际/港澳台</option>
								</select>
								<p class="help-block">短信消息类型：国内、国际/港澳台</p>
							</div>
						</div>
						<div class="cdn_bd <?php if($config['code_switch'] != '1'): ?>cdnhide<?php endif; ?>" id="code_switch_1">
							<div class="form-group">
								<label for="input-aly_keydi" class="col-sm-2 control-label">阿里云AccessKey ID</label>
								<div class="col-md-6 col-sm-10">
									<input type="text" class="form-control" id="input-aly_keydi" name="options[aly_keydi]" value="<?php echo (isset($config['aly_keydi']) && ($config['aly_keydi'] !== '')?$config['aly_keydi']:''); ?>">  
                                    <p class="help-block">阿里云控制台==》云通信-》短信服务==》 AccessKey ID</p>
								</div>
							</div>
							

							<div class="form-group">							
								<label for="input-aly_secret" class="col-sm-2 control-label">阿里云AccessKey Secret</label>
								<div class="col-md-6 col-sm-10">
									<input type="text" class="form-control" id="input-aly_secret" name="options[aly_secret]" value="<?php echo (isset($config['aly_secret']) && ($config['aly_secret'] !== '')?$config['aly_secret']:''); ?>">  
                                    <p class="help-block">阿里云控制台==》云通信-》短信服务==》 AccessKey Secret</p>
								</div>
							</div>
							

							<div class="form-group">
								<label for="input-aly_signName" class="col-sm-2 control-label">国内短信签名</label>
								<div class="col-md-6 col-sm-10">
									<input type="text" class="form-control" id="input-aly_signName" name="options[aly_signName]" value="<?php echo (isset($config['aly_signName']) && ($config['aly_signName'] !== '')?$config['aly_signName']:''); ?>">  
                                    <p class="help-block">阿里云控制台==》云通信-》短信服务==》 短信签名（区分国内、国际/港澳台）</p>
								</div>
							</div>

							<div class="form-group">							
								<label for="input-aly_templateCode" class="col-sm-2 control-label">国内短信模板ID</label>
								<div class="col-md-6 col-sm-10">
									<input type="text" class="form-control" id="input-aly_templateCode" name="options[aly_templateCode]" value="<?php echo (isset($config['aly_templateCode']) && ($config['aly_templateCode'] !== '')?$config['aly_templateCode']:''); ?>">  
                                    <p class="help-block">阿里云控制台==》云通信-》短信服务==》 短信模板ID （区分国内、国际/港澳台）</p> 
								</div>
							</div>
							
							<div class="form-group">
								<label for="input-aly_signName_inter" class="col-sm-2 control-label">国际/港澳台短信签名</label>
								<div class="col-md-6 col-sm-10">
									<input type="text" class="form-control" id="input-aly_signName_inter" name="options[aly_signName_inter]" value="<?php echo (isset($config['aly_signName_inter']) && ($config['aly_signName_inter'] !== '')?$config['aly_signName_inter']:''); ?>">  
                                    <p class="help-block">阿里云控制台==》云通信-》短信服务==》 短信签名（区分国内、国际/港澳台）</p>
								</div>
							</div>

							<div class="form-group">							
								<label for="input-aly_templateCode_inter" class="col-sm-2 control-label">国际/港澳台短信模板ID</label>
								<div class="col-md-6 col-sm-10">
									<input type="text" class="form-control" id="input-aly_templateCode_inter" name="options[aly_templateCode_inter]" value="<?php echo (isset($config['aly_templateCode_inter']) && ($config['aly_templateCode_inter'] !== '')?$config['aly_templateCode_inter']:''); ?>">  
                                    <p class="help-block">阿里云控制台==》云通信-》短信服务==》 短信模板ID （区分国内、国际/港澳台）</p>
								</div>
							</div>
						
						</div>
						
						
						 <div class="cdn_bd <?php if($config['code_switch'] != '2'): ?>cdnhide<?php endif; ?>" id="code_switch_2">
                            <div class="form-group">
                                <label for="input-tencent_sms_appid" class="col-sm-2 control-label">腾讯云短信SMS-AppID</label>
                                <div class="col-md-6 col-sm-10">
                                    <input type="text" class="form-control" id="input-tencent_sms_appid" name="options[tencent_sms_appid]" value="<?php echo (isset($config['tencent_sms_appid']) && ($config['tencent_sms_appid'] !== '')?$config['tencent_sms_appid']:''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input-tencent_sms_appkey" class="col-sm-2 control-label">腾讯云短信SMS-AppKey</label>
                                <div class="col-md-6 col-sm-10">
                                    <input type="text" class="form-control" id="input-tencent_sms_appkey" name="options[tencent_sms_appkey]" value="<?php echo (isset($config['tencent_sms_appkey']) && ($config['tencent_sms_appkey'] !== '')?$config['tencent_sms_appkey']:''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input-tencent_sms_signName" class="col-sm-2 control-label">腾讯云短信国内签名</label>
                                <div class="col-md-6 col-sm-10">
                                    <input type="text" class="form-control" id="input-tencent_sms_signName" name="options[tencent_sms_signName]" value="<?php echo (isset($config['tencent_sms_signName']) && ($config['tencent_sms_signName'] !== '')?$config['tencent_sms_signName']:''); ?>">  
                                    <p class="help-block">腾讯云控制台==》短信-》国内短信==》 签名管理</p>
                                </div>
                            </div>

                            <div class="form-group">                            
                                <label for="input-tencent_sms_templateCode" class="col-sm-2 control-label">腾讯云国内短信模板ID</label>
                                <div class="col-md-6 col-sm-10">
                                    <input type="text" class="form-control" id="input-tencent_sms_templateCode" name="options[tencent_sms_templateCode]" value="<?php echo (isset($config['tencent_sms_templateCode']) && ($config['tencent_sms_templateCode'] !== '')?$config['tencent_sms_templateCode']:''); ?>">  
                                    <p class="help-block">腾讯云控制台==》短信-》国内短信==》 正文模板管理</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input-tencent_sms_hw_signName" class="col-sm-2 control-label">腾讯云短信国外签名</label>
                                <div class="col-md-6 col-sm-10">
                                    <input type="text" class="form-control" id="input-tencent_sms_hw_signName" name="options[tencent_sms_hw_signName]" value="<?php echo (isset($config['tencent_sms_hw_signName']) && ($config['tencent_sms_hw_signName'] !== '')?$config['tencent_sms_hw_signName']:''); ?>">  
                                    <p class="help-block">腾讯云控制台==》短信-》国际/港澳台短信==》 签名管理</p>
                                </div>
                            </div>

                            <div class="form-group">                            
                                <label for="input-tencent_sms_hw_templateCode" class="col-sm-2 control-label">腾讯云国外短信模板ID</label>
                                <div class="col-md-6 col-sm-10">
                                    <input type="text" class="form-control" id="input-tencent_sms_hw_templateCode" name="options[tencent_sms_hw_templateCode]" value="<?php echo (isset($config['tencent_sms_hw_templateCode']) && ($config['tencent_sms_hw_templateCode'] !== '')?$config['tencent_sms_hw_templateCode']:''); ?>">  
                                    <p class="help-block">腾讯云控制台==》短信-》国际/港澳台短信==》 正文模板管理</p>
                                </div>
                            </div>
                        
                        </div>
					
                        <div class="form-group">
                            <label for="input-sendcode_switch" class="col-sm-2 control-label">短信验证码开关</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[sendcode_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['sendcode_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                                <p class="help-block">短信验证码开关,关闭后不再发送真实验证码，采用默认验证码123456</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-iplimit_switch" class="col-sm-2 control-label">短信验证码IP限制开关</label>
                            <div class="col-md-6 col-sm-10">
								<select class="form-control" name="options[iplimit_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['iplimit_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-iplimit_times" class="col-sm-2 control-label">短信验证码IP限制次数</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-iplimit_times" name="options[iplimit_times]" value="<?php echo (isset($config['iplimit_times']) && ($config['iplimit_times'] !== '')?$config['iplimit_times']:''); ?>"> 
                                <p class="help-block">同一IP每天可以发送验证码的最大次数</p>
                            </div>
                        </div>
                        
                        

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="1">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="B">
						
                        
                        <div class="form-group">
                            <label for="input-aliapp_partner" class="col-sm-2 control-label">支付宝合作者身份ID</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-aliapp_partner" name="options[aliapp_partner]" value="<?php echo (isset($config['aliapp_partner']) && ($config['aliapp_partner'] !== '')?$config['aliapp_partner']:''); ?>"> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-aliapp_seller_id" class="col-sm-2 control-label">支付宝帐号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-aliapp_seller_id" name="options[aliapp_seller_id]" value="<?php echo (isset($config['aliapp_seller_id']) && ($config['aliapp_seller_id'] !== '')?$config['aliapp_seller_id']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-aliapp_key" class="col-sm-2 control-label">支付宝开发者密钥</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-aliapp_key" name="options[aliapp_key]" ><?php echo (isset($config['aliapp_key']) && ($config['aliapp_key'] !== '')?$config['aliapp_key']:''); ?></textarea> 
                                <p class="help-block">密钥使用PSCS8版本</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-aliapp_check" class="col-sm-2 control-label" style="font-weight: normal;padding-top: 0;">----------------</label>
                            <div class="col-md-6 col-sm-10">
                                --------------------------------------------------------------------
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label for="input-wx_appid" class="col-sm-2 control-label">微信开放平台移动应用AppID</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-wx_appid" name="options[wx_appid]" value="<?php echo (isset($config['wx_appid']) && ($config['wx_appid'] !== '')?$config['wx_appid']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-wx_appsecret" class="col-sm-2 control-label">微信开放平台移动应用appsecret</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-wx_appsecret" name="options[wx_appsecret]" value="<?php echo (isset($config['wx_appsecret']) && ($config['wx_appsecret'] !== '')?$config['wx_appsecret']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-wx_mchid" class="col-sm-2 control-label">微信商户号mchid</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-wx_mchid" name="options[wx_mchid]" value="<?php echo (isset($config['wx_mchid']) && ($config['wx_mchid'] !== '')?$config['wx_mchid']:''); ?>"> 
                                <p class="help-block">微信商户号mchid（微信开放平台移动应用 对应的微信商户 商户号mchid）</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-wx_key" class="col-sm-2 control-label">微信密钥key</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-wx_key" name="options[wx_key]" value="<?php echo (isset($config['wx_key']) && ($config['wx_key'] !== '')?$config['wx_key']:''); ?>"> 
                                <p class="help-block">微信密钥key（微信开放平台移动应用 对应的微信商户 密钥key）</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-aliapp_check" class="col-sm-2 control-label" style="font-weight: normal;padding-top: 0;">-----------------</label>
                            <div class="col-md-6 col-sm-10">
                                --------------------------------------------------------------------
                            </div>
                        </div>
                        
        
                        <div class="form-group">
                            <label for="input-aliapp_switch" class="col-sm-2 control-label">APP-支付宝开关</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[aliapp_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['aliapp_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-wx_switch" class="col-sm-2 control-label">APP-微信开关</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[wx_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['wx_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="C">
                        <div class="form-group">
                            <label for="input-cash_rate" class="col-sm-2 control-label">提现比例</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-cash_rate" name="options[cash_rate]" value="<?php echo (isset($config['cash_rate']) && ($config['cash_rate'] !== '')?$config['cash_rate']:''); ?>"> 
                                <p class="help-block">提现一元人民币需要的票数</p>
                            </div>
                        </div>
						
						<div class="form-group">
							<label for="input-cash_take" class="col-sm-2 control-label">提现抽成（元）</label>
							<div class="col-md-6 col-sm-10">
								<input type="text" class="form-control" id="input-cash_take" name="options[cash_take]" value="<?php echo (isset($config['cash_take']) && ($config['cash_take'] !== '')?$config['cash_take']:''); ?>">
								<p class="help-block">
                                    (%-整数)百分比<br/>说明: 当提现比例设置为100映票等于1元时,提现抽成比例设置为10%,那么用户提现1000映票时,通过提现比例转换为10元,平台在从10元的基础上抽成10%,用户最终提现到账金额为9元</p>
							</div>
						</div>
                        
                        <div class="form-group">
                            <label for="input-cash_min" class="col-sm-2 control-label">提现最低额度（元）</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-cash_min" name="options[cash_min]" value="<?php echo (isset($config['cash_min']) && ($config['cash_min'] !== '')?$config['cash_min']:''); ?>"> 
                                <p class="help-block">可提现的最小额度，低于该额度无法提现</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-cash_start" class="col-sm-2 control-label">每月提现期</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-cash_start" name="options[cash_start]" value="<?php echo (isset($config['cash_start']) && ($config['cash_start'] !== '')?$config['cash_start']:''); ?>" style="width:100px;display: inline-block;">
                                -
                                <input type="text" class="form-control" id="input-cash_end" name="options[cash_end]" value="<?php echo (isset($config['cash_end']) && ($config['cash_end'] !== '')?$config['cash_end']:''); ?>" style="width:100px;display: inline-block;"> 
                                <p class="help-block">每月提现期限（日），不在时间段无法提现  例：10-15</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-cash_max_times" class="col-sm-2 control-label">每月提现次数</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-cash_max_times" name="options[cash_max_times]" value="<?php echo (isset($config['cash_max_times']) && ($config['cash_max_times'] !== '')?$config['cash_max_times']:'0'); ?>"> 
                                <p class="help-block">每月可提现最大次数，0表示不限制</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-cash_tip" class="col-sm-2 control-label">温馨提示</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-cash_tip" name="options[cash_tip]" value="<?php echo (isset($config['cash_tip']) && ($config['cash_tip'] !== '')?$config['cash_tip']:''); ?>" maxlength='50'> 
                                <p class="help-block">提现页面下部提示,最多50字</p>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="D">
                        <div class="form-group">
                            <label for="input-im_sdkappid" class="col-sm-2 control-label">腾讯云通信SdkAppId</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-im_sdkappid" name="options[im_sdkappid]" value="<?php echo (isset($config['im_sdkappid']) && ($config['im_sdkappid'] !== '')?$config['im_sdkappid']:''); ?>"> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-im_key" class="col-sm-2 control-label">腾讯云通信密钥</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-im_key" name="options[im_key]" value="<?php echo (isset($config['im_key']) && ($config['im_key'] !== '')?$config['im_key']:''); ?>"> 
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label for="input-im_admin" class="col-sm-2 control-label">腾讯云通信账号管理员</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-im_admin" name="options[im_admin]" value="<?php echo (isset($config['im_admin']) && ($config['im_admin'] !== '')?$config['im_admin']:''); ?>"> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-im_full_group_id" class="col-sm-2 control-label">腾讯云通信在线成员广播大群群组ID</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-im_full_group_id" name="options[im_full_group_id]" value="<?php echo (isset($config['im_full_group_id']) && ($config['im_full_group_id'] !== '')?$config['im_full_group_id']:''); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="E">
                        <div class="form-group">
                            <label for="input-conversa_time_min" class="col-sm-2 control-label">通话最低时长</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-conversa_time_min" name="options[conversa_time_min]" value="<?php echo (isset($config['conversa_time_min']) && ($config['conversa_time_min'] !== '')?$config['conversa_time_min']:'5'); ?>" onkeyup="if(this.value.replace(/\D/g,'')!=''){this.value=parseInt(this.value.replace(/\D/g,''))}else{this.value='0'}"> 
                                <p class="help-block">分钟  用户余额必须大于 规定时长的费用才能发起通话.0表示不限制</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-tx_appid" class="col-sm-2 control-label">直播appid</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tx_appid" name="options[tx_appid]" value="<?php echo (isset($config['tx_appid']) && ($config['tx_appid'] !== '')?$config['tx_appid']:''); ?>"> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-tx_bizid" class="col-sm-2 control-label">直播bizid</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tx_bizid" name="options[tx_bizid]" value="<?php echo (isset($config['tx_bizid']) && ($config['tx_bizid'] !== '')?$config['tx_bizid']:''); ?>"> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-tx_push" class="col-sm-2 control-label">直播推流域名</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tx_push" name="options[tx_push]" value="<?php echo (isset($config['tx_push']) && ($config['tx_push'] !== '')?$config['tx_push']:''); ?>"> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-tx_pull" class="col-sm-2 control-label">直播播流域名</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tx_pull" name="options[tx_pull]" value="<?php echo (isset($config['tx_pull']) && ($config['tx_pull'] !== '')?$config['tx_pull']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-tx_push_key" class="col-sm-2 control-label">直播推流防盗链Key</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tx_push_key" name="options[tx_push_key]" value="<?php echo (isset($config['tx_push_key']) && ($config['tx_push_key'] !== '')?$config['tx_push_key']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-tx_acc_key" class="col-sm-2 control-label">直播低延迟Key</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tx_acc_key" name="options[tx_acc_key]" value="<?php echo (isset($config['tx_acc_key']) && ($config['tx_acc_key'] !== '')?$config['tx_acc_key']:''); ?>">
                                <p class="help-block"> 一般是 直播推流防盗链Key</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-tx_api_key" class="col-sm-2 control-label">直播API鉴权key</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tx_api_key" name="options[tx_api_key]" value="<?php echo (isset($config['tx_api_key']) && ($config['tx_api_key'] !== '')?$config['tx_api_key']:''); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="F">
                        <div class="form-group">
                            <label for="input-match_voice" class="col-sm-2 control-label">匹配-语音价格</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-match_voice" name="options[match_voice]" value="<?php echo (isset($config['match_voice']) && ($config['match_voice'] !== '')?$config['match_voice']:''); ?>" onkeyup="if(this.value.replace(/\D/g,'')!=''){this.value=parseInt(this.value.replace(/\D/g,''))}else{this.value='0'}">
                                <p class="help-block"><?php echo $configpub['name_coin']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
					
					<!-- 云存储设置 -->
                    <div class="tab-pane" id="I">
					
						<div class="form-group">
                            <label for="input-cloudtype" class="col-sm-2 control-label">选择存储方式</label>
                            <div class="col-md-6 col-sm-10" id="cloudtype">
                                <label class="radio-inline"><input type="radio" value="1" name="options[cloudtype]" <?php if(in_array(($config['cloudtype']), explode(',',"1"))): ?>checked="checked"<?php endif; ?>>七牛云存储</label>
                               
                            </div>
                        </div>
						<div id="cloudtype_1" class="cloudtype_hide <?php if($config['cloudtype'] != '1'): ?>hide<?php endif; ?>">
                       
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                    <!-- 敏感词设置 -->
                    <div class="tab-pane" id="K">

                        <div class="form-group">
                            <label for="input-sensitive_words" class="col-sm-2 control-label">敏感词</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-sensitive_words" name="options[sensitive_words]" ><?php echo (isset($config['sensitive_words']) && ($config['sensitive_words'] !== '')?$config['sensitive_words']:''); ?></textarea><p class="help-block">设置多个敏感字，请用英文状态下逗号隔开</p>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
 
                </div>
            </div>
        </fieldset>
    </form>

</div>
<script type="text/javascript" src="/static/js/admin.js"></script>
<script>
(function(){
	//云存储切换
	$("#cloudtype label.radio-inline").on('click',function(){
			var v=$("input",this).val();
	
			
			$(".cloudtype_hide").addClass('hide');
			
			$("#cloudtype_"+v).removeClass('hide');
		
	})

	
    $("#cdn label").on('click',function(){
        var v_d=$("input",this).attr('disabled');
        if(v_d=='disabled'){
            return !1;
        }
        var v=$("input",this).val();
        var b=$("#code_switch_"+v);
        $(".cdn_bd").hide();
        b.show();
    })
    
})()
</script>
</body>
</html>
