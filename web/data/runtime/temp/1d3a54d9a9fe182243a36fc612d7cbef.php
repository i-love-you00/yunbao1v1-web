<?php /*a:2:{s:86:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/user/admin_index/index.html";i:1646881836;s:77:"/data/wwwroot/git1v1.yyyybbbb.com/themes/admin_simpleboot3/public/header.html";i:1646881836;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style type="text/css">

.item {
float: left;
clear: both;
margin-bottom: 5px;
}
/* 向上的箭头 */

.arrow-top {
font-size: 0;
line-height: 0;
border-width: 5px;
border-color: #18BC9C;
border-top-width: 0;
border-style: dashed;
border-bottom-style: solid;
border-left-color: transparent;
border-right-color: transparent;
}

/* 向下的箭头 */

.arrow-bottom {
font-size: 0;
line-height: 0;
border-width: 5px;
border-color: #18BC9C;
border-bottom-width: 0;
border-style: dashed;
border-top-style: solid;
border-left-color: transparent;
border-right-color: transparent;
}


.fl{
    float: left;
}
.fr{
    float: right;
}
.clear{
    clear: both;
}
</style>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a><?php echo lang('USER_INDEXADMIN_INDEX'); ?></a></li>
        <li><a href="<?php echo url('adminIndex/add'); ?>">添加主播</a></li>
    </ul>
    <form name="search" class="well form-inline margin-top-20" method="post" action="<?php echo url('user/adminIndex/index'); ?>">
        性别：
            <select class="form-control" name="sex" style="width: 100px;">
                <option value=''>不限</option>
                <option value="1" <?php if(input('request.sex') != '' && input('request.sex') == 1): ?>selected<?php endif; ?>>男</option>
                <option value="2" <?php if(input('request.sex') != '' && input('request.sex') == 2): ?>selected<?php endif; ?>>女</option>
                
            </select>
        注册时间：
            <input class="form-control js-bootstrap-date" name="start_time" id="start_time" value="<?php echo input('request.start_time'); ?>" aria-invalid="false" style="width: 110px;"> - 
            <input class="form-control js-bootstrap-date" name="end_time" id="end_time" value="<?php echo input('request.end_time'); ?>" aria-invalid="false" style="width: 110px;">
        用户ID：
        <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
               placeholder="请输入用户ID">
        关键字：
        <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
               placeholder="用户名/昵称/手机">
        <input class="form-control" type="hidden" name="coin_status" value="<?php echo input('request.coin_status'); ?>" >
        <input class="form-control" type="hidden" name="consumption_status" value="<?php echo input('request.consumption_status'); ?>" >
        <input class="form-control" type="hidden" name="votes_status" value="<?php echo input('request.votes_status'); ?>" >
        <input class="form-control" type="hidden" name="votestotal_status" value="<?php echo input('request.votestotal_status'); ?>" >
        <input class="form-control" type="hidden" name="createtime_status" value="<?php echo input('request.createtime_status'); ?>" >

        <input type="submit" class="btn btn-primary" value="搜索"/>
        <a class="btn btn-danger" href="<?php echo url('user/adminIndex/index'); ?>">清空</a>
		<br>
		<br>
		用户数：<?php echo $nums; ?>  (根据条件统计)
    </form>
    <form method="post" class="js-ajax-form">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th><?php echo lang('USERNAME'); ?></th>
                <th><?php echo lang('NICENAME'); ?></th>
                <th><?php echo lang('AVATAR'); ?></th>
                <th>性别</th>
                <th class="coin_top">
                    <p class="fl"><?php echo $configpub['name_coin']; ?></p>
                    <p class="fr">
                        <?php if($coin_status != '1'): ?><span class="item arrow-top" title="从低到高排序"></span><?php endif; if($coin_status != '-1'): ?><span class="item arrow-bottom" title="从高到底排序"></span><?php endif; ?>
                    </p>
                    <p class="clear"></p>
                    
                </th>
                <th class="consumption_top">
                    <p class="fl">总消费</p>
                    <p class="fr">
                        <?php if($consumption_status != '1'): ?><span class="item arrow-top" title="从低到高排序"></span><?php endif; if($consumption_status != '-1'): ?><span class="item arrow-bottom" title="从高到底排序"></span><?php endif; ?>
                    </p>
                    <p class="clear"></p>
                </th>
                <th class="votes_top">
                    <p class="fl"><?php echo $configpub['name_votes']; ?></p>
                    <p class="fr">
                        <?php if($votes_status != '1'): ?><span class="item arrow-top" title="从低到高排序"></span><?php endif; if($votes_status != '-1'): ?><span class="item arrow-bottom" title="从高到底排序"></span><?php endif; ?>
                    </p>
                    <p class="clear"></p>
                </th>
                <th class="votestotal_top">
                    <p class="fl">总收益</p>
                    <p class="fr">
                        <?php if($votestotal_status != '1'): ?><span class="item arrow-top" title="从低到高排序"></span><?php endif; if($votestotal_status != '-1'): ?><span class="item arrow-bottom" title="从高到底排序"></span><?php endif; ?>
                    </p>
                    <p class="clear"></p>
                </th>
                <th>手机</th>
                <th>推荐值</th>
                <th>实名认证</th>
                <th>主播认证</th>
                <th class="createtime_top">
                    <p class="fl"><?php echo lang('REGISTRATION_TIME'); ?></p>
                    <p class="fr">
                        <?php if($createtime_status != '1'): ?><span class="item arrow-top" title="正序排序"></span><?php endif; if($createtime_status != '-1'): ?><span class="item arrow-bottom" title="倒序排序"></span><?php endif; ?>
                    </p>
                    <p class="clear"></p>
                </th>
                <th><?php echo lang('LAST_LOGIN_TIME'); ?></th>
                <th><?php echo lang('LAST_LOGIN_IP'); ?></th>
                <th><?php echo lang('STATUS'); ?></th>
                <th><?php echo lang('ACTIONS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php 
                $user_statuses=array("0"=>lang('USER_STATUS_BLOCKED'),"1"=>lang('USER_STATUS_ACTIVATED'),"2"=>lang('USER_STATUS_UNVERIFIED'),"3"=>'已注销');
                $user_auth=array('0'=>'×','1'=>'√');
                $author_auth=array('0'=>'×','1'=>'√');
             if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo !empty($vo['user_login']) ? $vo['user_login'] : ($vo['mobile']?$vo['mobile']:lang('THIRD_PARTY_USER')); ?>
                    </td>
                    <td><?php echo !empty($vo['user_nickname']) ? $vo['user_nickname'] : lang('NOT_FILLED'); ?></td>
                    <td><img width="25" height="25" src="<?php echo get_upload_path($vo['avatar']); ?>"/></td>
                    <td><?php if($vo['sex'] == 1): ?>男<?php elseif($vo['sex'] == '2'): ?>女<?php else: ?>未设置<?php endif; ?></td>
                    <td><?php echo $vo['coin']; ?></td>
                    <td><?php echo $vo['consumption']; ?></td>
                    <td><?php echo $vo['votes']; ?></td>
                    <td><?php echo $vo['votestotal']; ?></td>
                    <td><?php echo $vo['mobile']; ?></td>
                    <td><?php echo $vo['recommend_val']; ?></td>
                    <td><?php echo $user_auth[$vo['isauth']]; ?></td>
                    <td><?php echo $author_auth[$vo['isauthor_auth']]; ?></td>
                    <td><?php echo date('Y-m-d H:i:s',$vo['create_time']); ?></td>
                    <td><?php if($vo['last_login_time'] > 0): ?><?php echo date('Y-m-d H:i:s',$vo['last_login_time']); ?><?php endif; ?></td>
                    <td><?php echo $vo['last_login_ip']; ?></td>
                    <td>
                        <?php switch($vo['user_status']): case "0": ?>
                                <span class="label label-danger"><?php echo $user_statuses[$vo['user_status']]; ?></span>
                            <?php break; case "1": ?>
                                <span class="label label-success"><?php echo $user_statuses[$vo['user_status']]; ?></span>
                            <?php break; case "2": ?>
                                <span class="label label-warning"><?php echo $user_statuses[$vo['user_status']]; ?></span>
                            <?php break; case "3": ?>
                                <span class="label label-warning"><?php echo $user_statuses[$vo['user_status']]; ?></span>
                            <?php break; ?>
                        <?php endswitch; ?>
                    </td>
                    <td>
					
						<?php if($vo['user_status'] != '3'): if($vo['isauth']): ?>
							    
							    <a class="btn btn-xs btn-danger js-ajax-dialog-btn"
								   href="<?php echo url('adminIndex/cancelUserAuth',array('id'=>$vo['id'])); ?>"
								   data-msg="确定要取消实名认证吗？<br />取消后主播认证也将被取消。">取消实名认证
                                </a>
							<?php endif; if($vo['isauthor_auth']): ?>
                                <a class="btn btn-xs btn-info  setrecommend" data-uid="<?php echo $vo['id']; ?>" data-value="<?php echo $vo['recommend_val']; ?>">推荐值</a>
                                <a class="btn btn-xs btn-danger js-ajax-dialog-btn"
                                   href="<?php echo url('adminIndex/cancelAuthorAuth',array('id'=>$vo['id'])); ?>"
                                   data-msg="确定要取消主播认证吗？">取消主播认证
                                </a>
                            <?php endif; if(empty($vo['user_status']) || (($vo['user_status'] instanceof \think\Collection || $vo['user_status'] instanceof \think\Paginator ) && $vo['user_status']->isEmpty())): ?>
								<a class="btn btn-xs btn-success js-ajax-dialog-btn"
								   href="<?php echo url('adminIndex/cancelban',array('id'=>$vo['id'])); ?>"
								   data-msg="<?php echo lang('ACTIVATE_USER_CONFIRM_MESSAGE'); ?>"><?php echo lang('ACTIVATE_USER'); ?></a>
						<?php else: ?>
								<a class="btn btn-xs btn-warning js-ajax-dialog-btn"
								   href="<?php echo url('adminIndex/ban',array('id'=>$vo['id'])); ?>"
								   data-msg="<?php echo lang('BLOCK_USER_CONFIRM_MESSAGE'); ?>"><?php echo lang('BLOCK_USER'); ?></a>
							<?php endif; ?>
						<?php endif; ?>
                        <a class="btn btn-xs btn-primary" href='<?php echo url("adminIndex/edit",array("id"=>$vo["id"])); ?>'><?php echo lang('EDIT'); ?></a>
                        <a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('adminIndex/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        <div class="pagination"><?php echo $page; ?></div>
    </form>
</div>
<script src="/static/js/admin.js"></script>
    <script>
        $(function(){

            Wind.use('layer');
            $('.setrecommend').click(function(){
                var _this=$(this);
                var uid=_this.data('uid');
                var recommend=_this.data('value');

                layer.prompt({
                    formType: 0,
                    title: '推荐值',
                    value: recommend,
                }, function(value, index, elem){
                    $.ajax({
                        url:'<?php echo url('user/adminIndex/setrecommend'); ?>',
                        type:'POST',
                        data:{uid:uid,recommend:value},
                        dataType:'json',
                        success:function(data){
                            var code=data.code;
                            if(code==0){
                                layer.msg(data.msg);
                                return !1;
                            }
                            layer.msg(data.msg,{},function(){
                                layer.closeAll();
                                location.reload();
                            });
                            
                        },
                        error:function(){
                            layer.msg('操作失败，请重试')
                        }
                    });
                    
                });
                
            });

            //钻石由低到高排序
            $(".coin_top .arrow-top").on('click',function(){
                //layer.msg("钻石向上");
                change_status('coin','1');
            });
            //钻石由高到低排序
            $(".coin_top .arrow-bottom").on('click',function(){
                //layer.msg("钻石向下");
                change_status('coin','-1');
            });

            //总消费由低到高排序
            $(".consumption_top .arrow-top").on('click',function(){
                //layer.msg("总消费向上");
                change_status('consumption','1');
            });

            //总消费由高到低排序
            $(".consumption_top .arrow-bottom").on('click',function(){
                //layer.msg("总消费向下");
                change_status('consumption','-1');
            });

            //映票由低到高排序
            $(".votes_top .arrow-top").on('click',function(){
                //layer.msg("映票向上");
                change_status('votes','1');
            });

            //映票由高到低排序
            $(".votes_top .arrow-bottom").on('click',function(){
                //layer.msg("映票向下");
                change_status('votes','-1');
            });

            
            //总收益由低到高排序
            $(".votestotal_top .arrow-top").on('click',function(){
                //layer.msg("总收益向上");
                change_status('votestotal','1');
            });

            //总收益由高到低排序
            $(".votestotal_top .arrow-bottom").on('click',function(){
                //layer.msg("总收益向下");
                change_status('votestotal','-1');
            });

            //注册时间正序排序
            $(".createtime_top .arrow-top").on('click',function(){
                //layer.msg("正序");
                change_status('createtime','1');
            });

            //注册时间倒序排序
            $(".createtime_top .arrow-bottom").on('click',function(){
                //layer.msg("倒序");
                change_status('createtime','-1');
            });
        })

        function change_status(current,val){
            $("input[name='"+current+"_status']").val(val);
            var arr=['coin','consumption','votes','votestotal','createtime'];
            for (var i = 0; i < arr.length; i++) {
                if(arr[i]!=current){
                    $("input[name='"+arr[i]+"_status']").val('');
                }
            }

            $("form[name='search']").submit();
        }
    </script>
</body>
</html>