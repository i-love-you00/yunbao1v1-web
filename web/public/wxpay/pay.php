<?php
/* *
 * 功能：支付宝手机网站支付接口(alipay.trade.wap.pay)接口调试入口页面
 * 版本：2.0
 * 修改日期：2016-11-01
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 请确保项目文件有可写权限，不然打印不了日志。
 */

header("Content-type: text/html; charset=utf-8");


require_once "../../sdk/zfbpay/wappay/service/AlipayTradeService.php";
require_once "../../sdk/zfbpay/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php";
require_once "../../sdk/zfbpay/config.php";
if (!empty($_GET['WIDout_trade_no'])&& trim($_GET['WIDout_trade_no'])!=""){
	//$config=getConfigPub();
    //商户订单号，商户网站订单系统中唯一订单号，必填
    $out_trade_no = $_GET['WIDout_trade_no'];

    //订单名称，必填
    $subject = $_GET['WIDsubject'];
	
	//订单名称，必填
    //$coin = $_POST['coin'];
	
	//$subject=$subject.$coin.$config['name_coin'];

    //付款金额，必填
    $total_amount = $_GET['total_amount'];

    //商品描述，可空
    $body = $_GET['body'];

    //超时时间
    $timeout_express="1m";

    $payRequestBuilder = new AlipayTradeWapPayContentBuilder();
    $payRequestBuilder->setBody($body);
    $payRequestBuilder->setSubject($subject);
    $payRequestBuilder->setOutTradeNo($out_trade_no);
    $payRequestBuilder->setTotalAmount($total_amount);
    $payRequestBuilder->setTimeExpress($timeout_express);

    $payResponse = new AlipayTradeService($config);
    $result=$payResponse->wapPay($payRequestBuilder,$config['return_url'],$config['notify_url']);


 }else{
	 echo "订单不存在";
 }

?>
