<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2021002102672162",

		//商户私钥，您的原始格式RSA私钥
		'merchant_private_key' => "MIIEpAIBAAKCAQEAyfB2u4bLYYP58KCNegsVq1SPlrIwNTWyxRrtWE6smXeD5BX182bm3w9jfL/rsJrfk2c4vkn6p4TmvpKOv+NMrjGwZx/YgB//XQ/ti3CmfzrQewwbXtsDolieHsb39YfuNXqe0H28HeS9M++985+tJHQRyL483RZilsdS5mo8Z+8E9KH0jguJIgZFM3BGUrYF94Fk2r7IMzPQOa4oZGMRqI9xAeW6kbrYokyO2Xe6KV8EWWVIzDJ8DS4zj1mGJa5vPa4ydUO+gPxtBJmb0Q9p8m/Uq/Q78osILAI5qeZOavMwssJhsHPAmkkFnt4aPLsplJfXKfe3CGrbBm9etu1CkwIDAQABAoIBADx5QHVEFp/rT+wZiISl0SVvu1hboEgu/ow8Vht94/5JZjvlv7PWewkNqLNxHfnTR2TzInEJ4oBMMnWbmhgoz4vS4YrgZItqCaUq4e5CG/o5+LkAlSnA2MPDWgbJevQScumdAPZa0Pz6xRZKk03j4YvrHnBJWBy1WWzh4BeUdOaCi3+ciE1xJOM05Ie0P2uB6CEC3fSw5/Mlce6WkubXaXvwHN5bDVnu1Hcq3+AhbYnyB0edSjFpeSHJeulwzLgfrTabxb7n/ZI1VVhUUB8o/kwN/e4z4REmNHKy/TAnwIyya99XqhTFYLsPo7SrS6FOPiAER0ksNHAUVAZnhekO9nECgYEA5arhj+C6NtsuY6VizXhD07RU8jnD3DJ9V//C9q477tk9I7fNDwWG6oLzbxWTV1qHcB/1XcoKeuJ+M62b0oCTBUcTexzt2ReSw0+vQALMQQcgNl6g4UNVw13IQT1B2VW3Vy5eBAgBJpiwuETbYObxyYerfyuvF2Aao9It/eHZaMUCgYEA4Re281oNb8BdAsHGUN0kVDDSJSdY7yH04j8uj0UpjtJuY1PasHB4F64wDFe9s7bs2cd6YbmcPrEIKtYivOTi/pus5RF1U2VAw1RRD4Fx9/BKPpVN7/9wDMh+lTk2BLDPG63TWd6c8tsO8I2ZbfWg5PBGBEjZcT5Vo/saUhXKQ3cCgYBoZVqOqho7ITpe9SiKOWarbsdGK9fJ2fF79LhMpTp9AaZ2++9sKpj/NHMvRWFqTa5O21gPP+2vmhC/sOSptOQffAVw6t961OpTeX8x25O442JTFISQoKj1W0XR0eSCxCHIsTzvHCxZvQJnYHVQAOpbLx5XeiXmvDqfyMQ7KVRH+QKBgQDT2jAU5mLTFSf1Wk+++AQjQTfoRBpOJ5WGA5VcvumYRLIkxl2DxrchAo6ybzpalQZjCn5C0CEgWBqpxpU3V+3VWMvnZQEaFqhXr1oDz/9mLkCSmo+aHL5nKaUJ6ajPzWGAWaAca4sof8iV24/WkGJ4VzRu/+m3zcVo6wMg0uVljQKBgQDEZkNShuVI3lXBIHMPA5cmeNpxrIMWlpilhNj+qCzocJ4RjckiKGD5UZ1CoeEUoRpMq5vMmH+mBnsMt423ncKkjTpkizXCu4X8Xf27aFeYATC2ad7TN3PAE2ynpXQ4AdwBqomLJtG9b68sGX4K5IF85oVUkb4U8jsj/at0ELMjpg==",
		
		//异步通知地址
		'notify_url' => "http://shejiao.yunbaozhibo.com/appapi/Wxpay/notify_ali",
		
		//同步跳转
		//'return_url' => "http://hnwl.yunbaozb.com/zfbpay/return_url.php",
		//'return_url' => "qingqinglive://com.hongyangqq.phonelive",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg2rKQB0aFWz2qBPH66GbIR0ir1FAR1RKFiRCsMAJs7KsWZlIYUojgEPq1JST2ALKJR9vtIEgcYlkyFRYFJtisRobV1Q6Ha+vkRjH/gTvQdSgX5JzXu3i3GkoLOiKgdPVjMcAU78dw0vtu3ShZnPDCTIEpDC/tCDIq4EQlzE9wyG2mj76UpykDyZeOVeK0gHNN+RYmHgO6S0vOM3ZfzvBOXGLAiCLLRqxZxWhL6mBjrtx8sEl5UKWx06bXEqSjA4zgZH+HZUxfiUhGIzSjjH6A6Eber2vQ/gnzqzFzfjctgbBV726F+SwG6KSM/pSclYbro4ejjQQjhTFFvnfW5oygwIDAQAB",
		
	
);