<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Live extends PhalApi_Model_NotORM {
	/* 主播信息 */
	public function getLiveuidInfo($liveuid) {
		
		$info=DI()->notorm->user
				->select('online,isauth,isvoice,voice_value,isvideo,video_value,isdisturb,isauthor_auth')
				->where('id=? and user_type=2',$liveuid)
				->fetchOne();

		return $info;
	}
	
	/* 获取用户的通话记录 */
	public function getUserConversation($uid) {

		$info= DI()->notorm->conversa_log
                ->select('*')
                ->where('uid=? or liveuid=?',$uid,$uid)
                ->order('id desc')
                ->fetchOne();

		return $info;
	}

	/* 获取通话记录 */
	public function getConversation($uid,$liveuid,$showid) {

		$info= DI()->notorm->conversa_log
                ->select('*')
                ->where('uid=? and liveuid=? and showid=?',$uid,$liveuid,$showid)
                ->fetchOne();

		return $info;
	}
    
    /* 写入记录 */
	public function setConversation($data) {

		$result= DI()->notorm->conversa_log->insert($data);

		return $result;
	}
    
    /* 更新记录 */
	public function upConversation($uid,$liveuid,$showid,$data) {

		$result= DI()->notorm->conversa_log
            ->where('uid=? and liveuid=? and showid=?',$uid,$liveuid,$showid)
            ->update($data);
			
		return $result;
	}
    
    /* 更新通话收益 */
	public function upConTotal($uid,$liveuid,$showid,$total,$undivide_total) {

		$result= DI()->notorm->conversa_log
                    ->where('uid=? and liveuid=? and showid=?',$uid,$liveuid,$showid)
                    ->update(
                    	array(
                    		'total' => new NotORM_Literal("total + {$total}"),
                    		'undivide_total'=>new NotORM_Literal("undivide_total + {$undivide_total}")
                    	)
                    );

		return $result;
	}
    
    /* 更新礼物收益 */
	public function upConGift($uid,$liveuid,$showid,$total) {

		$result= DI()->notorm->conversa_log
                    ->where('uid=? and liveuid=? and showid=?',$uid,$liveuid,$showid)
                    ->update(array('gift' => new NotORM_Literal("gift + {$total}") ) );

		return $result;
	}

    /* 更新接听总数 */
	public function upTotal($liveuid,$type=0) {
        if($type==1){
            $result= DI()->notorm->answer_rate
                    ->where('uid = ? and  total>=1', $liveuid)
                    ->update(array('total' => new NotORM_Literal("total - 1") ) );
        }else{
            $result= DI()->notorm->answer_rate
                    ->where('uid = ? ', $liveuid)
                    ->update(array('total' => new NotORM_Literal("total + 1") ) );
            if(!$result){
                $result= DI()->notorm->answer_rate
                    ->insert(array('uid' => $liveuid,'total'=>1 ) );
            }
        }
		

		return $result;
	}

    /* 更新接听成功 */
	public function upAnswer($liveuid) {

		$result= DI()->notorm->answer_rate
                    ->where('uid = ? ', $liveuid)
                    ->update(array('answer' => new NotORM_Literal("answer + 1") ) );

		return $result;
	}

    /* 更新状态 */
	public function upOnline($uid,$online) {

		$result= DI()->notorm->user
                    ->where('id = ? ', $uid)
                    ->update(['online'=>$online]);

		return $result;
	}

    /* 消费记录 扣除 */
	public function reduceRecord($uid,$showid) {

		$result= DI()->notorm->user_coinrecord
                    ->where('action in (2,3) and touid = ? and showid=?', $uid,$showid)
                    ->update(['isdeduct'=>1]);

		return $result;
	}

	public function getConversationByShowid($showid) {

		$result= DI()->notorm->conversa_log
            ->where('showid=?',$showid)
            ->fetchOne();
			
		return $result;
	}

}
