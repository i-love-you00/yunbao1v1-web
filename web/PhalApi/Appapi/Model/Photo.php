<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Photo extends PhalApi_Model_NotORM {
    
    /* 我的照片 */
    public function myPhoto($uid,$status,$p) {
        
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where="uid={$uid} and status not in(2,3) ";
		if($status>0){
			$where.=" and status={$status}";
		}
		
		
		$list=DI()->notorm->photo
                    ->select('id,uid,thumb,views,status,isprivate,coin,reason')
                    ->where($where)
                    ->order('id desc')
                    ->limit($start,$pnum)
                    ->fetchAll();

		return $list;
	}
    
    /* 照片列表 */
	public function getPhotoList($where='',$p,$order='id desc') {
        
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

		$list=DI()->notorm->photo
                    ->select('id,uid,thumb,views,isprivate,coin')
                    ->where('status=1')
                    ->where($where)
                    ->order($order)
                    ->limit($start,$pnum)
                    ->fetchAll();

		return $list;
	}

    
	/* 发布照片 */
	public function setPhoto($data) {
		
		$rs=DI()->notorm->photo->insert($data);

		return $rs;
	}

	/* 更新 */
	public function upPhoto($uid,$photoid,$data) {
		$rs=DI()->notorm->photo->where('id=? and uid=?',$photoid,$uid)->update($data);
		return $rs;
	}

	/* 获取照片信息 */
	public function getPhoto($photoid,$where='status=1') {
		$info=DI()->notorm->photo
                ->select('id,uid,thumb,views,isprivate,coin')
				->where('id = ?',$photoid)
				->where($where)
				->fetchOne();
		return $info;
	}
    
    /* 观看 */
	public function addView($uid,$photoid) {
		$rs=DI()->notorm->photo
				->where("id = ? and uid!=?",$photoid,$uid)
				->update( array('views' => new NotORM_Literal("views + 1") ) );
		return $rs;
	}


    /* 删除 */
	public function delPhoto($photoid) {

		$rs=DI()->notorm->photo
				->where("id = '{$photoid}'")
				->delete();

		return $rs;
	}
    
}
