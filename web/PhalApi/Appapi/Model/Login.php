<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Login extends PhalApi_Model_NotORM {

	protected $fields='id,user_nickname,avatar,avatar_thumb,sex,signature,coin,consumption,birthday,user_status,login_type,last_login_time,goodnums,badnums,isauth,votestotal,is_firstlogin,isauthor_auth';

	/* 会员登录 */   	
    public function userLogin($where) {

		$info=DI()->notorm->user
				->select($this->fields)
				->where('user_type=2')
                ->where($where)
				->fetchOne();
                
        return $info; 
    }	
	
	/* 会员注册 */
    public function userReg($data=[]) {
        
        $nowtime=time();
        $user_pass='live1v1'.$nowtime;
        $user_pass=setPass($user_pass);

        $avatar='/default.png';
        $avatar_thumb='/default_thumb.png';

        $default=array(
            'user_pass' =>$user_pass,
            'signature' =>'这家伙很懒，什么都没留下',
            'avatar' =>$avatar,
            'avatar_thumb' =>$avatar_thumb,
            'last_login_ip' =>$_SERVER['REMOTE_ADDR'],
            'create_time' => $nowtime,
            'user_status' => 1,
            "user_type"=>2,//会员
            "is_firstlogin"=>1
        );
        
        $insert=array_merge($default,$data);
        
            
		$rs=DI()->notorm->user->insert($insert);
        
        $id=$rs['id'];
        
        $info=DI()->notorm->user
				->select($this->fields)
				->where('id=?',$id)
				->fetchOne();
                
		return $info;
    }
		

	/* 更新token 登陆信息 */
    public function updateToken($uid,$token,$data=array()) {
        
        $nowtime=time();
		$expiretime=$nowtime+60*60*24*150;

		DI()->notorm->user
			->where('id=?',$uid)
			->update(array('last_login_time' => $nowtime, "last_login_ip"=>$_SERVER['REMOTE_ADDR'] ));

		$token_info=array(
			'user_id'=>$uid,
			'token'=>$token,
			'expire_time'=>$expiretime,
			'create_time'=>$nowtime,
		);
        $isexist=DI()->notorm->user_token
			->where('user_id=?',$uid)
			->update( $token_info );
        if(!$isexist){
            DI()->notorm->user_token
                ->insert( $token_info );
        }
		
		setCache("token_".$uid,$token_info);		
        
		return 1;
    }	
	

}
