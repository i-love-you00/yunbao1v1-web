<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Charge extends PhalApi_Model_NotORM {
	/* 充值规则列表 */
	public function getChargeRules() {
		
		$rules=DI()->notorm->charge_rules
				->select('id,name,money,coin,coin_ios,coin_paypal,product_id,give')
				->order('list_order asc')
				->fetchAll();

		return $rules;
	}
    
    /* 获取充值规则 */
	public function getChargeRule($changeid) {
		
		$charge=DI()->notorm->charge_rules->select('*')->where('id=?',$changeid)->fetchOne();

		return $charge;
	}		

	/* 订单号 */
	public function setOrder($orderinfo) {

		$result= DI()->notorm->charge_user->insert($orderinfo);

		return $result;
	}

	/* 订单号 */
	public function updatePaymentStatus($uid,$nonce,$orderid,$money) {
			 
		$orderinfo=DI()->notorm->charge_user->where('orderno=? and money=? ',$orderid,$money)->fetchOne();
		
		if(!$orderinfo){
			return 1003;
		}
		
		if($orderinfo['status']!=0){
			return 0;
		}
	
		/* 更新会员虚拟币 */
        $coin=$orderinfo['coin']+$orderinfo['coin_give'];
        /* 更新用户余额 消费 */
		DI()->notorm->user
				->where('id = ?', $uid)
				->update( array('coin' => new NotORM_Literal("coin + {$coin}")));
        /* 更新 订单状态 */
       
        $data['status']=1;
		//$data['nonce']=$nonce;
		$data['trade_no']=$nonce;
		$configpri=getConfigPri();
		if($configpri['paypal_pattern_braintree']==0){
			$data['ambient']=0;
		}else{
			$data['ambient']=1;
		}
		
		
        DI()->notorm->charge_user->where("orderno=? ",$orderid)->update($data);
       
        
        $res=$this->setAgentProfit($uid,$orderinfo['coin']);
				
		return $res;
	}	

}
