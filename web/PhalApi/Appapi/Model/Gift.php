<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_Gift extends PhalApi_Model_NotORM {

	/* 礼物列表 */   	
    public function getGiftList() {

		$list=DI()->notorm->gift
			->select("id,type,name,needcoin,thumb")
			->order("list_order asc,id desc")
			->fetchAll();
		foreach($list as $k=>$v){
			$list[$k]['thumb']=get_upload_path($v['thumb']);
		}	

		return $list;
    }	
	
	/* 单个礼物信息 */
    public function getGiftInfo($id) {
        
        $info=DI()->notorm->gift
                    ->select("id,type,name,thumb,needcoin,swftype,swf,swftime")
					->where('id=?',$id)
					->fetchOne();
        if($info){
            $info['thumb']=get_upload_path($info['thumb']);
            $info['swf']=get_upload_path($info['swf']);
        }
                
		return $info;
    }

}
