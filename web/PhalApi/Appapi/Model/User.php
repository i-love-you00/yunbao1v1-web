<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Model_User extends PhalApi_Model_NotORM {
	
	/* 用户列表 */
	public function getUserList($uid,$p,$type){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		
		if($type==1){
			$where="0=0 and id!=?";
		}else if($type==2){
			$where="isauthor_auth=0 and id!=?";
		}else{
			$where="isauthor_auth=1 and id!=?";
		}
		$list=DI()->notorm->user
					->select("id,user_nickname,avatar,avatar_thumb,sex,isauth,online,consumption,votestotal,isauthor_auth")
					->where($where,$uid)
					->limit($start,$pnum)
                    ->order('create_time desc')
					->fetchAll();
			
		foreach($list as $k=>$v){
			$v['avatar']=get_upload_path($v['avatar']);
			$v['avatar_thumb']=get_upload_path($v['avatar_thumb']);

			if($v['isauthor_auth']==0){
				$v['level']=getLevel($v['consumption']);
			}else{
				$v['level']=getLevelanchor($v['votestotal']);
			}			
			$v['u2t']= isAttention($uid,$v['id']);
			
            $v['isvip']="1";
            $v['isauthor_auth']="1";
			$v['isblack']=0;
			$list[$k]=$v;
	   }
		
		return $list;
	}


	/* 用户全部信息 */
	public function getBaseInfo($uid) {
		$info=DI()->notorm->user
				->select("id,user_nickname,avatar,avatar_thumb,sex,signature,coin,votes,consumption,votestotal,birthday,goodnums,badnums,isauth,isvoice,voice_value,isvideo,video_value,isdisturb,isauthor_auth,audio,audio_length,height,weight,constellation,labelid,label,label_c,province,city,district,intr")
				->where('id=?  and user_type=2',$uid)
				->fetchOne();
				
		return $info;
	}

    /* 视频价格 */
    public function getVideo() {

		$list=DI()->notorm->fee_video
				->select('coin,level')
                ->order('coin asc')
				->fetchAll();
                
        return $list; 
    }
    /* 检测用户昵称 */
    public function checkNickname($uid,$name){
        $isexist=DI()->notorm->user
				->select("id")
				->where('id!=?  and user_nickname=?',$uid,$name)
				->fetchOne();
        if($isexist){
            return 1;
        }
        
        return 0;
    }
    
    /* 关注列表 */
	public function getFollowsList($uid,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
        
		$list=DI()->notorm->user_attention
					->select("touid")
					->where('uid=?',$uid)
					->limit($start,$pnum)
                    ->order('addtime desc')
					->fetchAll();
		
		return $list;
	}
    
	/* 粉丝列表 */
	public function getFansList($uid,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$list=DI()->notorm->user_attention
					->select("uid")
					->where('touid=?',$uid)
					->limit($start,$pnum)
                    ->order('addtime desc')
					->fetchAll();
		
		return $list;
	}
    
    /* 关注 */
	public function setAttent($uid,$touid){
        DI()->notorm->user_attention
            ->insert(array("uid"=>$uid,"touid"=>$touid,"addtime"=>time()));
        return 1;		 
	}
    
    /* 取消关注 */
	public function delAttent($uid,$touid){
        DI()->notorm->user_attention
            ->where('uid=? and touid=?',$uid,$touid)
            ->delete();
        return 0;
	}


    /* 用户信息更新 */
	public function upUserInfo($uid,$data){
        $rs=0;
        if($data){
            $rs=DI()->notorm->user
                ->where('id=? ',$uid)
                ->update($data);
        }
        
        return $rs;
	}

    /* 礼物柜 */
	public function getGiftCab($liveuid){
        $rs=[
            'total'=>'0',
            'nums'=>'0',
            'list'=>[],
        ];
        
        $total=DI()->notorm->user_coinrecord
                ->select("sum(nums) as total_nums,sum(totalcoin) as total_coin")
                ->where('type=0 and action=1 and touid=? ',$liveuid)
                ->fetchOne();
        if($total){
            if($total['total_nums']){
                $rs['nums']=$total['total_nums'];
            }
            if($total['total_coin']){
                $rs['total']=$total['total_coin'];
            }
        }
                
        $list=DI()->notorm->user_coinrecord
                ->select("actionid,sum(nums) as total_nums")
                ->where('type=0 and action=1 and touid=? ',$liveuid)
                ->group('actionid')
                ->order('total_nums desc')
                ->fetchAll();
        $rs['list']=$list;
        
        return $rs;
	}
    
	/* 个人主页 */
	public function getUserHome($uid) {
		$info=DI()->notorm->user
				->select("id,user_nickname,avatar,avatar_thumb,birthday,goodnums,badnums,isauth,isvoice,voice_value,isvideo,video_value,isdisturb,online,last_online_time,votestotal,isauthor_auth,audio,audio_length,height,weight,constellation,labelid,label,label_c,province,city,district,intr,signature,sex")
				->where('id=?  and user_type=2',$uid)
				->fetchOne();
		return $info;
	}

	/* 接听 */
	public function getAnswerRate($uid) {
		$info=DI()->notorm->answer_rate
				->select("*")
				->where('uid=? ',$uid)
				->fetchOne();
		return $info;
	}
	

	

    //
    public function getUserMaterial($uid){
    	$userinfo=DI()->notorm->user
    		->select('id,user_nickname,avatar,avatar_thumb,audio,audio_length,sex,height,weight,constellation,labelid,label,label_c,province,city,district,intr,signature')
    		->where(['user_type'=>2,'id'=>$uid])
    		->fetchOne();

    	return $userinfo;

    }
	
}
