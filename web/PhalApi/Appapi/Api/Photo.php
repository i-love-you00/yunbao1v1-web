<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 相册
 */

class Api_Photo extends PhalApi_Api {

	public function getRules() {
        return array(
            'myPhoto' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'status' => array('name' => 'status',  'type' => 'int', 'default'=>'','desc' => '相册状态'),
				'p' => array('name' => 'p', 'type' => 'int', 'desc' => '页码'),
			),
            
            'setPhoto' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'thumb' => array('name' => 'thumb', 'type' => 'string', 'desc' => '封面'),
				'isprivate' => array('name' => 'isprivate', 'type' => 'int', 'desc' => '是否私密，0否1是'),
				'coin' => array('name' => 'coin', 'type' => 'int', 'desc' => '价格'),
				'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名 uid   thumb   isprivate'),
			),
            
            'setPublic' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'photoid' => array('name' => 'photoid', 'type' => 'int', 'desc' => '照片ID'),
			),
            
            'addView' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'photoid' => array('name' => 'photoid', 'type' => 'int', 'desc' => '照片ID'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名 uid  photoid '),
			),

            'delPhoto' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'photoid' => array('name' => 'photoid', 'type' => 'int', 'desc' => '照片ID'),
			),
            
            'getHomePhoto' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'desc' => '主播ID'),
				'p' => array('name' => 'p', 'type' => 'int', 'desc' => '页码'),
			),
            
        );
	}
	
    /**
     * 我的照片
     * @desc 用于获取我的照片
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[].id 照片ID  
     * @return string info[].title 标题  
     * @return string info[].thumb 封面
     * @return string info[].views 观看数  
     * @return string info[].isprivate 是否私密，0否1是  
     * @return string info[].coin 价格
     * @return string info[].status 审核状态，0审核中，1通过，2拒绝
     * @return string info[].reason 失败原因
     * @return string msg 提示信息
     */
    //
	public function myPhoto() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$status=checkNull($this->status);
		$p=checkNull($this->p);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        
        $domain = new Domain_Photo();
		$list=$domain->myPhoto($uid,$status,$p);
        
        $rs['info']=$list;
        
        return $rs;
	}

    /**
     * 发布照片
     * @desc 用于用户发布照片
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
    //
	public function setPhoto() {
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$thumb=checkNull($this->thumb);
		$isprivate=0;
		$coin=checkNull($this->coin);
		$sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $thumb=='' ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'thumb'=>$thumb,
            'isprivate'=>$isprivate,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1002;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $data=[
            'uid'=>$uid,
            'thumb'=>$thumb,
            'isprivate'=>$isprivate,
            'coin'=>$coin,
        ];
        
        $domain = new Domain_Photo();
		$result=$domain->setPhoto($data);
        
		return $result;
        
	}
    
    /**
     * 观看
     * @desc 用于更新照片观看量
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[].nums 数量
     * @return string msg 提示信息
     */
    //
	public function addView() {
        
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$photoid=checkNull($this->photoid);
		$sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $photoid <1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'photoid'=>$photoid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1002;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        
        $domain = new Domain_Photo();
		$result=$domain->addView($uid,$photoid);
        
		return $result;
        
	}

    /**
     * 删除照片
     * @desc 用于用户删除照片
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
	public function delPhoto() {
        
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$photoid=checkNull($this->photoid);
        
        if($uid<1 || $token=='' || $photoid <1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

        $domain = new Domain_Photo();
		$result=$domain->delPhoto($uid,$photoid);
        
		return $result;
        
	}

    /**
     * 个人照片
     * @desc 用于获取个人主页照片
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[].id 照片ID  
     * @return string info[].thumb 封面  
     * @return string info[].views 观看数  
     * @return string info[].isprivate 是否私密，0否1是  
     * @return string info[].coin 价格  
     * @return string info[].cansee 是否能看，0否1是  
     * @return string msg 提示信息
     */
    //
	public function getHomePhoto() {
        
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$liveuid=checkNull($this->liveuid);
		$p=checkNull($this->p);
        
        if($uid<1 || $token=='' || $liveuid <1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }

        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        
        $domain = new Domain_Photo();
		$result=$domain->getHomePhoto($uid,$liveuid,$p);
        
		return $result;
        
	}

    
}
