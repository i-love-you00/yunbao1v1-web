<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 注册登录
 */
error_reporting(0);
session_start();
class Api_Login extends PhalApi_Api { 
	public function getRules() {
        return array(
		
			'userLogin' => array(
                'user_login' => array('name' => 'user_login', 'type' => 'string', 'desc' => '账号'),
				'country_code' => array('name' => 'country_code', 'type' => 'string', 'desc' => '账号-区号'),
				'code' => array('name' => 'code', 'type' => 'string', 'desc' => '验证码'),
				'source' => array('name' => 'source', 'type' => 'int', 'default'=>'0', 'desc' => '来源设备,1android'),
            ),
			
			'getCode' => array(
				'mobile' => array('name' => 'mobile', 'type' => 'string', 'desc' => '手机号'),
				'country_code' => array('name' => 'country_code', 'type' => 'string', 'desc' => '手机号-区号'),
                'sign' => array('name' => 'sign', 'type' => 'string',  'default'=>'', 'desc' => '签名'),
			),
			
			'setUserOnline' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户Token'),
				'online' => array('name' => 'online', 'type' => 'int','default'=>'0', 'desc' => '状态，0离线，1勿扰，2在聊，3在线'),
            ),
        );
	}
	
	
    
    /**
     * 登录方式开关信息
     * @desc 用于获取登录方式开关信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[][0] 登录方式标识

     * @return string msg 提示信息
     */
    public function getLoginType() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $configpri=getConfigPri();
		
		$info = getConfigPub();
		//隐私弹窗
        $login_alert=array(
            'title'=>$info['login_alert_title'],
            'content'=>$info['login_alert_content'],
            'login_title'=>$info['login_clause_title'],
            'message'=>array(
                array(
                    'title'=>$info['login_service_title'],
                    'url'=>get_upload_path($info['login_service_url']),
                ),
                array(
                    'title'=>$info['login_private_title'],
                    'url'=>get_upload_path($info['login_private_url']),
                ),
            )
        );


        $rs['info'][0]['login_alert'] = $login_alert;

        return $rs;
    }
	
	
    /**
     * 会员登陆
     * @desc 用于用户登陆信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nickname 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].level_anchor 主播等级
     * @return string info[0].birthday 生日
     * @return string info[0].token 用户Token
     * @return string info[0].isauth 是否用户认证，0否1是
     * @return string info[0].isauthor_auth 是否主播认证，0否1是
     * @return string info[0].usersig IM签名
     * @return string msg 提示信息
     */
    public function userLogin() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		$user_login=checkNull($this->user_login);
		$code=checkNull($this->code);
		$source=checkNull($this->source);
		$country_code=checkNull($this->country_code);

        if($user_login==''){
            $rs['code'] = 1001;
            $rs['msg'] = '请输入手机号';
            return $rs;	
        }
        
        if($code==''){
            $rs['code'] = 1002;
            $rs['msg'] = '请输入验证码';
            return $rs;	
        }
        
        if(!isset($_SESSION['reg_mobile']) || !$_SESSION['reg_mobile'] || !isset($_SESSION['reg_mobile_code']) || !$_SESSION['reg_mobile_code'] ){
            $rs['code'] = 1003;
            $rs['msg'] = '请先获取验证码';
            return $rs;		
        }
		
		if($user_login!=$_SESSION['reg_mobile']){
            $rs['code'] = 1004;
            $rs['msg'] = '手机号码不一致';
            return $rs;					
		}
        
        if($country_code!=$_SESSION['reg_country_code']){
            $rs['code'] = 1004;
            $rs['msg'] = '国家区号不一致';
            return $rs;					
		}

		if($code!=$_SESSION['reg_mobile_code']){
            $rs['code'] = 1005;
            $rs['msg'] = '验证码错误';
            return $rs;					
		}
        
        $user_pass='live1v1'.time();
        
        $domain = new Domain_Login();
        $info = $domain->userLogin($user_login,$user_pass,$source,$country_code);

		if($info==1001){
			$rs['code'] = 1001;
            $rs['msg'] = '账号或密码错误';
            return $rs;	
		}else if($info==1002){
			$rs['code'] = 1002;
            $rs['msg'] = '该账号已被禁用';
            return $rs;	
		}else if($info==1003){
			$rs['code'] = 1003;
            $rs['msg'] = '该账号已被注销';
            return $rs;	
		}
	
        $rs['info'][0] = $info;
        
        unset($_SESSION['reg_mobile']);
        unset($_SESSION['reg_mobile_code']);
        unset($_SESSION['reg_mobile_expiretime']);
		
        return $rs;
    }		
	
	/**
	 * 获取注册短信验证码
	 * @desc 用于注册获取短信验证码
	 * @return int code 操作码，0表示成功,2发送失败
	 * @return array info 
	 * @return string msg 提示信息
	 */
	 
	public function getCode() {
		$rs = array('code' => 0, 'msg' => '发送成功，请注意查收', 'info' => array());
		
		$mobile = checkNull($this->mobile);
		$country_code = checkNull($this->country_code);
		
		$sign = checkNull($this->sign);
		
        if($mobile==''){
			$rs['code']=1001;
			$rs['msg']='请输入手机号';
			return $rs;
		}
        
		$ismobile=checkMobile($mobile,$country_code);
		
		if($ismobile==1001){
			$rs['code']=1001;
			$rs['msg']='国际/港澳台不支持国内区号';
			return $rs;
		}else if($ismobile==1002){
			$rs['code']=1002;
			$rs['msg']='国内不支持国际/港澳台区号';
			return $rs;	
		}else if(!$ismobile){
			$rs['code']=1001;
			$rs['msg']='请输入正确的手机号';
			return $rs;	
		}
        
        $checkdata=array(
            'mobile'=>$mobile
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;
        }
		
		$iscancellation=checkIsDestroyByLogin($mobile,'1');
		if($iscancellation){
			$rs['code']=1001;
			$rs['msg']='该账号已被注销,不能使用';
			return $rs;	
		}

		if($_SESSION['reg_mobile']==$mobile && $_SESSION['reg_mobile_expiretime']> time() ){
			$rs['code']=1002;
			$rs['msg']='验证码5分钟有效，请勿多次发送';
			return $rs;
		}
		
        $limit = ip_limit();
		if( $limit == 1){
			$rs['code']=1003;
			$rs['msg']='您已当日发送次数过多';
			return $rs;
		}
		$mobile_code = random(6,1);
		
		/* 发送验证码 */
 		$result=sendCode($mobile,$mobile_code,$country_code);
		if($result['code']==0){
			$_SESSION['reg_mobile'] = $mobile;
			$_SESSION['reg_mobile_code'] = $mobile_code;
			$_SESSION['reg_country_code'] = $country_code;
			$_SESSION['reg_mobile_expiretime'] = time() +60*5;	
		}else if($result['code']==667){
			$_SESSION['reg_mobile'] = $mobile;
            $_SESSION['reg_mobile_code'] = $result['msg'];
			$_SESSION['reg_country_code'] = $country_code;
            $_SESSION['reg_mobile_expiretime'] = time() +60*5;
            
            $rs['code']=1002;
			$rs['msg']='验证码为：'.$result['msg'];
		}else{
			$rs['code']=1002;
			$rs['msg']=$result['msg'];
		} 
		
		
		return $rs;
	}		
	
	
	/**
     * 用户状态
     * @desc 用于更新用户在线状态
     * @return int code 状态码,0表示成功
     * @return string msg 返回提示信息
     * @return array info 返回信息
     */
    public function setUserOnline(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $online=$this->online;
 

        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        if($online==''){
            $rs['code'] = 1001;
            $rs['msg'] = '参数错误';
            return $rs;
        }
		
		$domain=new Domain_Login();
        $domain->setUserOnline($uid,$online);
		
        return $rs;
    }

}
