<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 认证
 */

class Api_Auth extends PhalApi_Api {

	public function getRules() {
        return array(

            'getAuthStatus'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
            ),

            'setUserAuth'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
                'name' => array('name' => 'name', 'type' => 'string', 'desc' => '真实姓名'),
                'mobile' => array('name' => 'mobile', 'type' => 'string', 'desc' => '手机号码'),
                'cardno' => array('name' => 'cardno', 'type' => 'string', 'desc' => '身份证号'),
            ),

            'setAuthorAuth'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
                'thumb' => array('name' => 'thumb', 'type' => 'string', 'desc' => '封面图片'),
                'backwall' => array('name' => 'backwall', 'type' => 'string', 'desc' => '背景墙图片'),
                'video' => array('name' => 'video', 'type' => 'string', 'desc' => '个人介绍视频'),
                'video_thumb' => array('name' => 'video_thumb', 'type' => 'string', 'desc' => '个人介绍视频封面'),

            ),
        );
	}
    


    /**
     * 获取实名认证和主播认证的状态
     * @desc 获取实名认证和主播认证的状态
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     * @return string info[0]['auth_title'] 返回状态名称
     * @return string info[0]['auth_icon'] 返回状态图标
     * @return string info[0]['status'] 返回状态 -1 没有提交认证  0 审核中  1  通过  2 拒绝
     * */
    public function getAuthStatus(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);

        
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        $domain=new Domain_Auth();
        $res=$domain->getAuthStatus($uid);

        $rs['info'][0]=$res;
        return $rs;

    }

    /**
     * 用户实名认证
     * @desc 用户实名认证
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     * */
    public function setUserAuth(){
        $rs = array('code' => 0, 'msg' => '实名认证提交成功,请等待管理员审核', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $name=checkNull($this->name);
        $mobile=checkNull($this->mobile);
        $cardno=checkNull($this->cardno);

        
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        if($name==''){
            $rs['code']=1001;
            $rs['msg']="请填写真实姓名";
            return $rs;
        }

        if(mb_strlen($name)>6){
            $rs['code']=1002;
            $rs['msg']="真实姓名不能超过6个字";
            return $rs;
        }

        if(!$cardno){
            $rs['code']=1003;
            $rs['msg']="请输入身份证号";
            return $rs;
        }

        $check_cardno=isCreditNo($cardno);
        if(!$check_cardno){
            $rs['code']=1004;
            $rs['msg']="身份证号不合法";
            return $rs;
        }

        if(!$mobile){
            $rs['code']=1005;
            $rs['msg']="请填写手机号码";
            return $rs;
        }

        $check_mobile=checkMobile($mobile,'86');
        if($check_mobile==1001){
            $rs['code']=1006;
            $rs['msg']="只能填写非中国大陆手机号码";
            return $rs;
        }

        if($check_mobile==1002){
            $rs['code']=1007;
            $rs['msg']="只能填写中国大陆手机号码";
            return $rs;
        }

        if(!$check_mobile){
            $rs['code']=1008;
            $rs['msg']="手机号码格式错误";
            return $rs;
        }

        $domain=new Domain_Auth();
        $post_data=array(
            'name'=>$name,
            'cardno'=>$cardno,
            'mobile'=>$mobile
        );
        $res=$domain->setUserAuth($uid,$post_data);
        if($res==1001){
            $rs['code']=1009;
            $rs['msg']="实名认证审核中,请等待管理员审核";
            return $rs;
        }

        if($res==1002){
            $rs['code']=1010;
            $rs['msg']="实名认证已审核通过无需重复认证";
            return $rs;
        }

        if(!$res){
            $rs['code']=1011;
            $rs['msg']="实名认证失败";
            return $rs;
        }

        return $rs;
    }

    /**
     * 主播认证
     * @desc 主播认证
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     * */
    public function setAuthorAuth(){
        $rs = array('code' => 0, 'msg' => '主播认证提交成功,请等待管理员审核', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $thumb=checkNull($this->thumb);
        $backwall=checkNull($this->backwall);
        $video=checkNull($this->video);
        $video_thumb=checkNull($this->video_thumb);

        
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        if($thumb==''){
            $rs['code'] =1001;
            $rs['msg'] = '请上传封面图';
            return $rs;
        }

        if($backwall==''){
            $rs['code'] =1002;
            $rs['msg'] = '请上传至少一张背景墙图片';
            return $rs;
        }

        $backwall_a=array_filter( preg_split('/,|，/',$backwall) );

        if(!$backwall_a){
            $rs['code'] = 1002;
            $rs['msg'] = '请上传至少一张背景墙图片';
        }else if(count($backwall_a)>6){
            $rs['code'] = 1002;
            $rs['msg'] = '最多上传六张背景墙图片';
            return $rs;
        }

        $post_data=array(
            'thumb'=>$thumb,
            'backwall'=>$backwall,
            'video'=>$video,
            'video_thumb'=>$video_thumb,
        );

        $domain=new Domain_Auth();
        $res=$domain->setAuthorAuth($uid,$post_data);

        if($res==1001){
            $rs['code'] =1003;
            $rs['msg'] = '请先进行实名认证';
            return $rs;
        }

        if($res==1002){
            $rs['code'] =1004;
            $rs['msg'] = '实名认证审核中,请等待审核通过';
            return $rs;
        }

        if($res==1003){
            $rs['code'] =1005;
            $rs['msg'] = '实名认证失败,请重新进行实名认证';
            return $rs;
        }

        if($res==1004){
            $rs['code'] =1006;
            $rs['msg'] = '主播认证审核中,请等待管理员审核';
            return $rs;
        }

        if($res==1005){
            $rs['code'] =1007;
            $rs['msg'] = '主播认证已审核通过无需重复认证';
            return $rs;
        }

        if(!$res){
            $rs['code']=1008;
            $rs['msg']="主播认证失败";
            return $rs;
        }

        return $rs;

    }
    
    
}



