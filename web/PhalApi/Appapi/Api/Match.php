<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

/**
 * 匹配
 */

class Api_Match extends PhalApi_Api {

	public function getRules() {
        return array(
            'getMatch' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
			),
            
            'check' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
                'type' => array('name' => 'type', 'type' => 'int', 'desc' => '类型，1视频2语音'),
			),
            
            'userMatch' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'type' => array('name' => 'type', 'type' => 'int', 'desc' => '类型，1视频2语音'),
			),
            
            'userCancel' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
			),
            
            'anchorMatch' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
                'type' => array('name' => 'type', 'type' => 'int', 'desc' => '类型，1视频2语音'),
			),
            
            'anchorCancel' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
			),
			
        );
	}
	
    /**
     * 匹配配置
     * @desc 用于获取匹配设置
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[].isauth 是否主播认证，0否1是
     * @return string info[].isuser_auth 是否实名认证，0否1是
     * @return string info[].voice 语音价格 
     * @return string info[].voice_vip 语音-VIP价格  
     * @return string info[].video 视频价格
     * @return string info[].video_vip 视频-VIP价格  
     * @return string msg 提示信息
     */
    //
	public function getMatch() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $isauth=isAuthorAuth($uid);
        $isuser_auth=isAuth($uid);
        
        $configpri=getConfigPri();
        
        $info=[
            'voice'=>$configpri['match_voice'],
            'voice_vip'=>$configpri['match_voice_vip'],
            'video'=>$configpri['match_video'],
            'video_vip'=>$configpri['match_video_vip'],
            'isauth'=>$isauth,
            'isuser_auth'=>$isuser_auth,
        ];
        
        $rs['info'][0]=$info;
        
        return $rs;
	}

    /**
     * 匹配检测
     * @desc 用于用户[非主播]开始匹配前检测信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
    //
	public function check() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$type=checkNull($this->type);
        
        if($uid<1 || $token=='' || ($type!=1 && $type !=2) ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $isauth=isAuthorAuth($uid);
        if($isauth){
            $rs['code']=1003;
			$rs['msg']='您已通过主播认证，请返回首页重新匹配';
			return $rs;
        }
        
        $configpri=getConfigPri();
        
        

        $userinfo=getUserCoin($uid);
        if($userinfo['coin'] < $total*1){
            $rs['code'] = 1008;
            $rs['msg'] = '余额不足1分钟通话，请先充值';
            return $rs;
        }
        
        return $rs;
	}

    /**
     * 用户匹配
     * @desc 用于用户开始匹配
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
    //
	public function userMatch() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$type=checkNull($this->type);
        
        if($uid<1 || $token=='' || ($type!=1 && $type !=2) ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $configpri=getConfigPri();
        

        $userinfo=getUserCoin($uid);
        if($userinfo['coin'] < $total*1){
            $rs['code'] = 1008;
            $rs['msg'] = '余额不足1分钟通话，请先充值';
            return $rs;
        }

        
        
        $key_user='match_user_'.$type;
        $key_anchor='match_anchor_'.$type; 
        
        $anchorid=lPop($key_anchor);
        if(!$anchorid){
            rPush($key_user,$uid);
            return $rs;
        }
		
        
        
        $domain = new Domain_Match();
		$result=$domain->match($uid,$anchorid,$type);
        
        return $result;
	}

    /**
     * 用户取消
     * @desc 用于用户取消配置
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
    //
	public function userCancel() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        $key_user1='match_user_1';
        $key_user2='match_user_2';
        lRem($key_user1,$uid);
        lRem($key_user2,$uid);
        
        return $rs;
	}

    /**
     * 主播匹配
     * @desc 用于主播开始匹配
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
	public function anchorMatch() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$type=checkNull($this->type);
        
        if($uid<1 || $token=='' || ($type!=1 && $type !=2) ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

        
        $domain = new Domain_Match();
        /* 检测开关 */
        $userinfo=$domain->getLiveuidInfo($uid);
 
        
        if($type==1 && !$userinfo['isvideo']){
            $rs['code'] = 1005;
			$rs['msg'] = '您未开启视频接听';
			return $rs;
        }
        
        if($type==2 && !$userinfo['isvoice']){
            $rs['code'] = 1006;
			$rs['msg'] = '您未开启语音接听';
			return $rs;
        }
        
        
        $key_user='match_user_'.$type;
        $key_anchor='match_anchor_'.$type; 
        $userid=lPop($key_user);
        if(!$userid){
			rPush($key_anchor,$uid);
            return $rs;
        }
		
		
        
		$result=$domain->match($userid,$uid,$type);
        
        return $result;

	}

    /**
     * 主播取消
     * @desc 用于主播取消匹配
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
	public function anchorCancel() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $key_anchor1='match_anchor_1';
        $key_anchor2='match_anchor_2';
        lRem($key_anchor1,$uid);
        lRem($key_anchor2,$uid);
        
        return $rs;
	}

	

    
    
    
    
    
}
