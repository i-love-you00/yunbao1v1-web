<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_Charge {

    /* 充值规则 */
	public function getChargeRules() {

        $key='getChargeRules';
		$rules=getcaches($key);
		if(!$rules){
            $model = new Model_Charge();
			$rules= $model->getChargeRules();
			setcaches($key,$rules);
		}

		return $rules;
	}

    /* 生成订单 */
	public function setOrder($changeid,$orderinfo) {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());

		$model = new Model_Charge();
        
        $charge = $model->getChargeRule($changeid);

        if(!$charge){
            $rs['code']=1003;
			$rs['msg']='信息错误';
			return $rs;
        }
        
        
        if($charge['money']!=$orderinfo['money'] || ($charge['coin']!=$orderinfo['coin']  && $orderinfo['coin']!=$charge['coin_ios']   && $orderinfo['coin']!=$charge['coin_paypal'] )){
			$rs['code']=1003;
			$rs['msg']='信息错误';
			return $rs;
		}
		
		$orderinfo['coin_give']=$charge['give'];
        
		$rs = $model->setOrder($orderinfo);

		return $rs;
	}
	
	public function updatePaymentStatus($uid,$nonce,$orderid,$money) {
		$rs = array();

		$model = new Model_Charge();
		
		$rs = $model->updatePaymentStatus($uid,$nonce,$orderid,$money);

		return $rs;
	}
	
}
