<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————

class Domain_Home {
    /* 轮播 */
    public function getSlide($slide_id=1) {
        $rs = array();

        $key='getSlide_'.$slide_id;
        $list=getcaches($key);
        if(!$list){
            $model = new Model_Home();
            $list = $model->getSlide($slide_id);
            if($list){
                setcaches($key,$list);
            }
        }

        return $list;
    }
    /* 热门 */
    public function getList($uid,$sex,$type,$p) {
        
        if($uid<1){
            $uid=0;
        }
        $where=[];
        $where['id !=?']=$uid;
        if($sex==1){
            $where['sex']=1;
        }
        
        if($sex==2){
            $where['sex !=?']=1;
        }
        
        if($type==1){
            $where['isvideo']=1;
        }
        
        if($type==2){
            $where['isvoice']=1;
        }
        
        $model = new Model_Home();
        $list = $model->getList($where,$p);
        
        $domain_wall = new Domain_Wall();
        
        foreach($list as $k=>$v){
			$v['level_anchor']=getLevelanchor($v['votestotal']);
			$v['avatar']=get_upload_path($v['avatar']);

            $authinfo=$domain_wall->myWall($v['id']);
            $thumb=$v['avatar'];
            if($authinfo['thumb']){
                $thumb=$authinfo['thumb'];
            }
            $v['thumb']=$thumb;

			unset($v['goodnums']);
			unset($v['badnums']);
            
            $list[$k]=$v;
		}

        return $list;
    }

    /* 搜索 */
    public function search($uid,$key,$p) {

        $model = new Model_Home();
        $list = $model->search($uid,$key,$p);
                
        foreach($list as $k=>$v){
			$v['level']=getLevel($v['consumption']);
			$v['level_anchor']=getLevelanchor($v['votestotal']);
			/* $v['level_anchor']=getLevelanchor($v['goodnums'],$v['badnums']); */
			$v['avatar']=get_upload_path($v['avatar']);
			$v['fans']=getFans($v['id']);
			unset($v['consumption']);
			unset($v['votestotal']);
			unset($v['goodnums']);
			unset($v['badnums']);
            
			$v['isblack']=0;
            
            $list[$k]=$v;
		}


        return $list;
    }

	
	/*获取收益(魅力)排行榜*/
	public function profitList($uid,$type,$p){
		
		switch ($type) {
			case 'day':
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

			break;

			case 'week':
                $w=date('w'); 
                //获取本周开始日期，如果$w是0，则表示周日，减去 6 天 
                $first=1;
                //周一
                $week=date('Y-m-d H:i:s',strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days')); 
                $week_start=strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days'); 

                //本周结束日期 
                //周天
                $week_end=strtotime("{$week} +1 week")-1;
                
				$where=" addtime >={$week_start} and addtime<={$week_end} and ";

			break;

			case 'month':
                //本月第一天
                $month=date('Y-m-d',strtotime(date("Ym").'01'));
                $month_start=strtotime(date("Ym").'01');

                //本月最后一天
                $month_end=strtotime("{$month} +1 month")-1;

				$where=" addtime >={$month_start} and addtime<={$month_end} and ";

			break;

			case 'total':
				$where=" ";
			break;
			
			default:
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";
			break;
		}
		//type:0:支出；1：收入； action：1：收礼物；2：视频通话，3：语音通话
		$where.=" type='0'  and action in (1,2,3)";
		$model = new Model_Home();
		$result=$model->profitList($where,$p);
	
		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nickname']=$userinfo['user_nickname'];
			$v['sex']=$userinfo['sex'];
			$v['level']=$userinfo['level'];
			$v['level_anchor']=$userinfo['level_anchor'];
            $v['isAttention']=isAttention($uid,$v['uid']);//判断当前用户是否关注了该主播
            $v['isAuth']=isAuth($v['uid']);//判断当前用户是否认证
            $result[$k]=$v;
		}
		return $result;
	}
	
	/*获取消费（土豪）排行榜*/
	public function consumeList($uid,$type,$p){
		switch ($type) {
			case 'day':
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

			break;
            
            case 'week':
                $w=date('w'); 
                //获取本周开始日期，如果$w是0，则表示周日，减去 6 天 
                $first=1;
                //周一
                $week=date('Y-m-d H:i:s',strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days')); 
                $week_start=strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days'); 

                //本周结束日期 
                //周天
                $week_end=strtotime("{$week} +1 week")-1;
                
				$where=" addtime >={$week_start} and addtime<={$week_end} and ";

			break;

			case 'month':
                //本月第一天
                $month=date('Y-m-d',strtotime(date("Ym").'01'));
                $month_start=strtotime(date("Ym").'01');

                //本月最后一天
                $month_end=strtotime("{$month} +1 month")-1;

				$where=" addtime >={$month_start} and addtime<={$month_end} and ";

			break;

			case 'total':
				$where=" ";
			break;
			
			default:
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";
			break;
		}

		// $where.=" type='0' and action='1'";
		$where.=" 1=1 ";
		
		$model = new Model_Home();
		$result=$model->consumeList($where,$p);
		
		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nickname']=$userinfo['user_nickname'];
			$v['sex']=$userinfo['sex'];
			$v['level']=$userinfo['level'];
			$v['level_anchor']=$userinfo['level_anchor'];
            
            $v['isAttention']=isAttention($uid,$v['uid']);//判断当前用户是否关注了该主播
            $v['isAuth']=isAuth($v['uid']);//判断当前用户是否认证
            $result[$k]=$v;
		}

		return $result;
	}
	
	//收益（魅力）榜：前三名
	public function getProfittop(){
		
		//获取今天开始结束时间
		$dayStart=strtotime(date("Y-m-d"));
		$dayEnd=strtotime(date("Y-m-d 23:59:59"));
		$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

		//type:0:支出；1：收入； action：1：收礼物；2：视频通话，3：语音通话
		// $where.=" type='0' and action ='1'";
		$where.=" type='0' ";
		
		$model = new Model_Home();
		$result=$model->getProfittop($where);
		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nickname']=$userinfo['user_nickname'];
            $result[$k]=$v;
		}
		return $result;
	}
	
	//消费（土豪）榜：前三名
	public function getConsumetop(){
	
		//获取今天开始结束时间
		$dayStart=strtotime(date("Y-m-d"));
		$dayEnd=strtotime(date("Y-m-d 23:59:59"));
		$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

		// $where.=" type='0' and action='1'";
		$where.=" 1=1 ";
		$model = new Model_Home();
		$result=$model->getConsumetop($where);
		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nickname']=$userinfo['user_nickname'];
            $result[$k]=$v;
		}
		return $result;
	}
	
}
