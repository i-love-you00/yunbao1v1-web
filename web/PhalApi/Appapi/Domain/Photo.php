<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_Photo {

    /* 我的照片 */
	public function myPhoto($uid,$status,$p) {

        $model = new Model_Photo();
        $list= $model->myPhoto($uid,$status,$p);
        
        foreach($list as $k=>$v){
            $v['thumb']=get_upload_path($v['thumb']);
            $v['views']=NumberFormat($v['views']);
            if($v['status']<2){
                $v['reason']='';
            }
            
            $list[$k]=$v;
        }
        
		return $list;
	}


    /* 发布照片 */
	public function setPhoto($data) {
		$rs = array('code' => 0, 'msg' => '发布成功，请等待审核', 'info' => array());
        
        $uid=$data['uid'];
        $thumb=$data['thumb'];
        
        $nowtime=time();
        if($data['isprivate']==1 && $data['coin']<=0){
            $rs['code']=1005;
			$rs['msg']='请选择价格';
			return $rs;
        }
        
        if($data['isprivate']==0){
            $data['coin']='0';
        }
        $data['addtime']=$nowtime;
        $data['uptime']=$nowtime;
        
		$model = new Model_Photo();
        $result = $model->setPhoto($data);

        if(!$result){
            $rs['code']=1004;
			$rs['msg']='发布失败，请重试';
			return $rs;
        }
        
		return $rs;
	}

    /* 观看 */
	public function addView($uid,$photoid) {
        
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());

		$model = new Model_Photo();
        
        $info = $model->getPhoto($photoid);
        if(!$info){
            $rs['code']=1003;
			$rs['msg']='照片不存在';
			return $rs;
        }
        
        if($info['uid']==$uid){
            
            $nums=$info['views'];
            $rs['info'][0]['nums']=NumberFormat($nums);
            return $rs;
        }
        
        $model->addView($uid,$photoid);

        $nums=$info['views']+1;
        
        $rs['info'][0]['nums']=NumberFormat($nums);

		return $rs;
	}


    /* 删除 */
	public function delPhoto($uid,$photoid) {
        
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        
		$model = new Model_Photo();
        $where="uid={$uid}";
        $info = $model->getPhoto($photoid,$where);
        if(!$info){
            $rs['code']=1003;
			$rs['msg']='照片不存在';
			return $rs;
        }
        
        if($info['uid']!=$uid){
            $rs['code']=1004;
			$rs['msg']='操作失败';
			return $rs;
        }
        
        $result = $model->delPhoto($photoid);

        if(!$result){
            $rs['code']=1005;
            $rs['msg']='操作失败';
            return $rs;
        }

		return $rs;
	}

    /* 个人照片 */
	public function getHomePhoto($uid,$liveuid,$p) {

        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        
		$model = new Model_Photo();
          
        $where=[];
        $where['uid']=$liveuid;
        
        $list = $model->getPhotoList($where,$p);
        
        if(!$list){
            $list=[];
        }

        if(!empty($list)){
            foreach($list as $k=>$v){
                $v['thumb']=get_upload_path($v['thumb']);
                $v['views']=NumberFormat($v['views']);
                $v['cansee']='0';
                if($v['uid']==$uid){
                    $v['cansee']='1';
                }
                $list[$k]=$v;
            }
        }
        

        $rs['info']=$list;

		return $rs;

	}

	
}
