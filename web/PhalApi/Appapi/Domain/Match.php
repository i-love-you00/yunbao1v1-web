<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_Match {
    /* 检测信息 */
	public function getLiveuidInfo($uid) {

        $model = new Model_Live();
        
        $liveuidinfo= $model->getLiveuidInfo($uid);
        
        return $liveuidinfo;
    }
    /* 匹配 */
	public function match($uid,$anchorid,$type) {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $configpri=getConfigPri();
        
        $fee=[
            '1'=>[
                '0'=>$configpri['match_video'],
                '1'=>$configpri['match_video_vip'],
            ],
            '2'=>[
                '0'=>$configpri['match_voice'],
                '1'=>$configpri['match_voice_vip'],
            ],
        ];
        
        $total=$fee[$type][0];
        
        $nowtime=time();
        $showid=$nowtime;
        $data=[
            'uid'=>$uid,
            'liveuid'=>$anchorid,
            'type'=>$type,
            'coin'=>$total,
            'starttime'=>$showid,
            'showid'=>$showid,
            'status'=>1,
        ];
        $model = new Model_Live();
        $result= $model->setConversation($data);
        
        /* 更新状态 */
        $model->upOnline($uid,2);
        $model->upOnline($anchorid,2);
        
        /* 流地址 */
        $stream=$uid.'_'.$showid;
        $stream_l=$anchorid.'_'.$showid;
		
		
		
        $push=PrivateKey_tx('rtmp',$stream,1);
        $pull=tx_acc('rtmp',$stream,0); 
				
        
        $push_l=PrivateKey_tx('rtmp',$stream_l,1);
        $pull_l=tx_acc('rtmp',$stream_l,0);
        
        $data=[
            'uid'=>$uid,
            'liveuid'=>$anchorid,
            'type'=>$type,
            'total'=>$total,
            'showid'=>$showid,
            'push'=>$push,
            'pull'=>$pull,
            'push_l'=>$push_l,
            'pull_l'=>$pull_l,
        ];

        $this->setIM($data);
		return $rs;

	}


    /* 发送私信 */
    public function setIM($info) {
        /* IM */
        /* 用户给主播发 */
        $userinfo=getUserInfo($info['uid']);
        $ext=[
            'method'=>'call',
            'action'=>'12',
            'type'=>$info['type'],
            'showid'=>$info['showid'],
            
            'id'=>$userinfo['id'],
            'avatar'=>$userinfo['avatar'],
            'user_nickname'=>$userinfo['user_nickname'],
            'level'=>$userinfo['level'],
            'level_anchor'=>$userinfo['level_anchor'],
            
            'push'=>$info['push_l'],
            'pull'=>$info['pull_l'],
        ];
        
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMCustomElem',       //自定义类型
            'MsgContent' => array(
                'Data' => json_encode($ext),
                'Desc' => '',
            )
        );
        //将创建的元素$msg_content_elem, 加入array $msg_content
        array_push($msg_content, $msg_content_elem);
        
        $account_id=(string)$info['uid'];
        $receiver=(string)$info['liveuid'];
        $api=getTxRestApi();

        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content);
        
        /* 主播给用户发 */
        $anchorinfo=getUserInfo($info['liveuid']);
        $ext=[
            'method'=>'call',
            'action'=>'12',
            'type'=>$info['type'],
            'showid'=>$info['showid'],
            
            'id'=>$anchorinfo['id'],
            'avatar'=>$anchorinfo['avatar'],
            'user_nickname'=>$anchorinfo['user_nickname'],
            'level'=>$anchorinfo['level'],
            'level_anchor'=>$anchorinfo['level_anchor'],
            
            'push'=>$info['push'],
            'pull'=>$info['pull'],
        ];
        
        #构造高级接口所需参数
        $msg_content = array();
        //创建array 所需元素
        $msg_content_elem = array(
            'MsgType' => 'TIMCustomElem',       //自定义类型
            'MsgContent' => array(
                'Data' => json_encode($ext),
                'Desc' => '',
            )
        );
        array_push($msg_content, $msg_content_elem);
        
        $account_id=(string)$info['liveuid'];
        $receiver=(string)$info['uid'];

        $ret = $api->openim_send_msg_custom($account_id, $receiver, $msg_content);
        
     
    }
	
}
