<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_Auth {


    /*获取实名认证和主播认证的状态*/
    public function getAuthStatus($uid){
        $model=new Model_Auth();
        $user_auth_info=$model->getUserAuthInfo($uid);
        $author_auth_info=$model->getAuthorAuthInfo($uid);

        $user_auth_base=[
            'auth_title'=>'实名认证',
            'auth_icon'=>get_upload_path('/static/app/auth/user_auth.png'),
        ];

        if(!$user_auth_info){
            $user_auth_info=[
                'status'=>'-1', //无
                
            ];
        }

        $user_auth_info['auth_type']='user_auth';
        $user_auth_info['addtime']=date("Y-m-d H:i:s",$user_auth_info['addtime']);
        if($user_auth_info['uptime']){
            $user_auth_info['uptime']=date("Y-m-d H:i:s",$user_auth_info['uptime']);
        }

        $user_auth_info_new=array_merge($user_auth_base,$user_auth_info);

        $author_auth_base=[
            'auth_title'=>'主播认证',
            'auth_icon'=>get_upload_path('/static/app/auth/author_auth.png'),
        ];

        if(!$author_auth_info){
           $author_auth_info=[
                'status'=>'-1', //无
            ];
        }
        
        $backwall_list=array_filter( preg_split('/,|，/', $author_auth_info['backwall']) );
        $backwall_list_format=[];
        foreach ($backwall_list as $k => $v) {
            $backwall_list_format[]=get_upload_path($v);
        }

        $author_auth_info['auth_type']='author_auth';
        $author_auth_info['thumb_format']=get_upload_path($author_auth_info['thumb']);
        $author_auth_info['backwall_list']=$backwall_list;
        $author_auth_info['backwall_list_format']=$backwall_list_format;
        $author_auth_info['video_format']=get_upload_path($author_auth_info['video']);
        $author_auth_info['video_thumb_format']=get_upload_path($author_auth_info['video_thumb']);
        $author_auth_info['addtime']=date("Y-m-d H:i:s",$author_auth_info['addtime']);
        if($author_auth_info['uptime']){
            $author_auth_info['uptime']=date("Y-m-d H:i:s",$author_auth_info['uptime']);
        }

        $author_auth_info_new=array_merge($author_auth_base,$author_auth_info);

        $result=array(
            'user_auth_status'=>$user_auth_info_new['status'],
            'author_auth_status'=>$author_auth_info_new['status'],
            'auth_info'=>[
                '0'=>$user_auth_info_new,
                '1'=>$author_auth_info_new
            ]
        );

        return $result;
    }

    //用户实名认证
    public function setUserAuth($uid,$post_data){

        $model=new Model_Auth();
        $user_auth_info=$model->getUserAuthInfo($uid);
        if($user_auth_info){
            $status=$user_auth_info['status'];
            if($status==0){
                return 1001;
            }

            if($status==1){
                return 1002;
            }

            if($status==2){
               $post_data['addtime']=time();
               $post_data['uptime']=0;
               $post_data['status']=0;
               $result=$model->upUserAuth($uid,$post_data);
               if(!$result){
                    return 0;
               }
            }
        }else{
            $post_data['uid']=$uid;
            $post_data['addtime']=time();
            $result=$model->setUserAuth($post_data);
            if(!$result){
                return 0;
            }
        }

        return 1;
    }

    //主播认证
    public function setAuthorAuth($uid,$post_data){
        $model=new Model_Auth();
        $user_auth_info=$model->getUserAuthInfo($uid);

        if(!$user_auth_info){
            return 1001;
        }

        $user_auth_status=$user_auth_info['status'];

        if($user_auth_status==0){
            return 1002;
        }

        if($user_auth_status==2){
            return 1003;
        }

        $author_auth_info=$model->getAuthorAuthInfo($uid);

        if($author_auth_info){

            $status=$author_auth_info['status'];
            if($status==0){
                return 1004;
            }

            if($status==1){
                return 1005;
            }

            if($status==2){
               $post_data['addtime']=time();
               $post_data['uptime']=0;
               $post_data['status']=0;
               $result=$model->upAuthorAuth($uid,$post_data);
               if(!$result){
                    return 0;
               }
            }

        }else{
            $post_data['uid']=$uid;
            $post_data['addtime']=time();
            $result=$model->setAuthorAuth($post_data);
            if(!$result){
                return 0;
            }
        }

        return 1;
    }
}
