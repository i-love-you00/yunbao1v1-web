<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_Live {

    /* 检测信息 */
	public function checklive($uid,$liveuid,$type) {
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $nowtime=time();
        

        $model = new Model_Live();
        
        $liveuidinfo= $model->getLiveuidInfo($liveuid);
        if(!$liveuidinfo){
            $rs['code'] = 1002;
			$rs['msg'] = '用户不存在';
			return $rs;
        }
        
        
        if($liveuidinfo['online']==2){
            $rs['code'] = 800;
			$rs['msg'] = '对方正在和其他人通话';
			return $rs;
        }
        if($liveuidinfo['online']==0){
            $rs['code'] = 800;
			$rs['msg'] = '对方离线，暂时无法接通';
			return $rs;
        }
        
        if($type==1 && !$liveuidinfo['isvideo']){
            $rs['code'] = 1005;
			$rs['msg'] = '对方未开启视频接听';
			return $rs;
        }
        
        if($type==2 && !$liveuidinfo['isvoice']){
            $rs['code'] = 1006;
			$rs['msg'] = '对方未开启语音接听';
			return $rs;
        }
        
        $userinfo=getUserCoin($uid);
        $total=0;
        if($type==1){
            $total=$liveuidinfo['video_value'];
        }
        
        if($type==2){
            $total=$liveuidinfo['voice_value'];
        }
        
        if(!$total){
            $rs['code'] = 1007;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        $configpri=getConfigPri();
        $conversa_time_min=$configpri['conversa_time_min'];
        if($conversa_time_min>0){
            if($total * $conversa_time_min > $userinfo['coin']){
                $rs['code'] = 1008;
                $rs['msg'] = '余额不足'.$conversa_time_min.'分钟通话，请先充值';
                return $rs;
            }
        }
        
        //owner
        $conversainfo= $model->getUserConversation($uid);
        if($conversainfo){
            if($conversainfo['status']==1){
                $rs['code'] = 1010;
                $rs['msg'] = '正在通话中';
                return $rs;
            }
            
            if($conversainfo['status']==0 && $nowtime - $conversainfo['showid'] < 60){
                $rs['code'] = 1011;
                $rs['msg'] = '正在通话中';
                return $rs;
            }
        }

        //side--new
        $live_conversainfo=$model->getUserConversation($liveuid);

        if($live_conversainfo){
            if($live_conversainfo['status']==1){
                $rs['code'] = 1010;
                $rs['msg'] = '正在通话中';
                return $rs;
            }
            
            if($live_conversainfo['status']==0 && $nowtime - $live_conversainfo['showid'] < 60){
                $rs['code'] = 1011;
                $rs['msg'] = '对方正在等待通话';
                return $rs;
            }
        }

        $key1="match_anchor_1";
        $key2="match_anchor_2";
        $video_arr=lRange($key1,0,-1);
        $audio_arr=lRange($key2,0,-1);

        if(in_array($liveuid, $video_arr) || in_array($liveuid, $audio_arr)){
            $rs['code'] = 1012;
            $rs['msg'] = '对方正在匹配,无法发起通话';
            return $rs;
        }
        
        $showid=$nowtime;
        $data=[
            'uid'=>$uid,
            'liveuid'=>$liveuid,
            'type'=>$type,
            'coin'=>$total,
            'showid'=>$showid,
            'status'=>0,
        ];
        
        $result= $model->setConversation($data);
        if(!$result){
            $rs['code'] = 1009;
			$rs['msg'] = '拨打失败，请重试';
			return $rs;
        }
        
        
        $model->upTotal($liveuid);
        
        /* 流地址 */
        $stream=$uid.'_'.$showid;
        $push=PrivateKey_tx('rtmp',$stream,1);
        $pull=tx_acc('rtmp',$stream,0);
		/* $pull=PrivateKey_tx('http',$stream,0);  */
        $info=[
            'type'=>$type,
            'total'=>$total,
            'showid'=>$showid,
            'push'=>$push,
            'pull'=>$pull,
        ];

        $rs['info'][0]=$info;
		return $rs;
	}

    /* 主播发起 */
	public function anchorLaunch($uid,$touid,$type) {
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $nowtime=time();
        
        $isblack=isBlack($uid,$touid);
        if($isblack){
            $rs['code'] = 1001;
			$rs['msg'] = '你已把对方拉黑，请先解除拉黑';
			return $rs; 
        }
        $isblack=isBlack($touid,$uid);
        if($isblack){
            $rs['code'] = 1001;
			$rs['msg'] = '对方已把你拉黑';
			return $rs; 
        }
        
        $model = new Model_Live();
        
        $uidinfo= $model->getLiveuidInfo($touid);
        if(!$uidinfo){
            $rs['code'] = 1002;
			$rs['msg'] = '用户不存在';
			return $rs;
        }
        
        if($uidinfo['isdisturb']){
            $rs['code'] = 1004;
			$rs['msg'] = '对方开启勿扰，无法邀请通话';
			return $rs;
        }
        
        if($uidinfo['online']==2){
            $rs['code'] = 1004;
			$rs['msg'] = '对方正在和其他人通话，是否预约';
			return $rs;
        }
        if($uidinfo['online']==0){
            $rs['code'] = 1004;
			$rs['msg'] = '对方离线，暂时无法接通';
			return $rs;
        }
        
        
        $liveuidinfo= $model->getLiveuidInfo($uid);
        if(!$liveuidinfo){
            $rs['code'] = 1002;
			$rs['msg'] = '用户不存在';
			return $rs;
        }
        
        if($liveuidinfo['isauthor_auth']!=1){
            $rs['code'] = 1003;
			$rs['msg'] = '您尚未认证成为主播';
			return $rs;
        }
        
        if($liveuidinfo['online']==2){
            $rs['code'] = 1004;
			$rs['msg'] = '正在和其他人通话';
			return $rs;
        }
        
        if($type==1 && !$liveuidinfo['isvideo']){
            $rs['code'] = 1005;
			$rs['msg'] = '您还未开启视频接听';
			return $rs;
        }
        
        if($type==2 && !$liveuidinfo['isvoice']){
            $rs['code'] = 1006;
			$rs['msg'] = '您还未开启语音接听';
			return $rs;
        }
        
        
        $userinfo=getUserCoin($touid);
        $total=0;
        if($type==1){
            $total=$liveuidinfo['video_value'];
        }
        
        if($type==2){
            $total=$liveuidinfo['voice_value'];
        }
        
        if(!$total){
            $rs['code'] = 1007;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $configpri=getConfigPri();
        $conversa_time_min=$configpri['conversa_time_min'];
        if($conversa_time_min>0){
            if($total * $conversa_time_min > $userinfo['coin']){
                $rs['code'] = 1008;
                $rs['msg'] = '对方余额不足'.$conversa_time_min.'分钟通话';
                return $rs;
            }
        }
        
        $conversainfo= $model->getUserConversation($touid);
        if($conversainfo){
            if($conversainfo['status']==1){
                $rs['code'] = 1010;
                $rs['msg'] = '对方正在通话中';
                return $rs;
            }
            
            if($conversainfo['status']==0 && $nowtime - $conversainfo['showid'] < 60){
                $rs['code'] = 1011;
                $rs['msg'] = '对方正在通话中';
                return $rs;
            }
        }

        //new
        $key1="match_user_1";
        $key2="match_user_2";
        $video_arr=lRange($key1,0,-1);
        $audio_arr=lRange($key2,0,-1);

        if(in_array($touid, $video_arr) || in_array($touid, $audio_arr)){
            $rs['code'] = 1012;
            $rs['msg'] = '对方正在匹配,无法发起通话';
            return $rs;
        }
        
        
        $showid=$nowtime;
        $data=[
            'uid'=>$touid,
            'liveuid'=>$uid,
            'type'=>$type,
            'coin'=>$total,
            'showid'=>$showid,
            'status'=>0,
        ];
        
        $result= $model->setConversation($data);
        if(!$result){
            $rs['code'] = 1009;
			$rs['msg'] = '拨打失败，请重试';
			return $rs;
        }
        
        //$model->upTotal($liveuid);
        
        /* 流地址 */
        $stream=$uid.'_'.$showid;
        $push=PrivateKey_tx('rtmp',$stream,1);
        $pull=tx_acc('rtmp',$stream,0);
		/* $pull=PrivateKey_tx('http',$stream,0); */
        $info=[
            'type'=>$type,
            'total'=>$total,
            'showid'=>$showid,
            'push'=>$push,
            'pull'=>$pull,
        ];

        $rs['info'][0]=$info;
		return $rs;
	}


    
    /* 主播接听 */
    public function anchorAnswer($uid,$touid,$showid){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$model = new Model_Live();
        
        $conversainfo = $model->getConversation($touid,$uid,$showid);
        if(!$conversainfo){
            $rs['code']=1002;
			$rs['msg']='信息错误';
			return $rs;
        }
        
        if($conversainfo['status']!=0){
            $rs['code']=1003;
			$rs['msg']='信息错误';
			if($conversainfo['status']==2){
				$rs['msg']='对方结束通话';
			}
			return $rs;
        }
        $nowtime=time();
        if($nowtime - $showid > 60 ){
            $rs['code']=1004;
			$rs['msg']='无效信息';
			return $rs;
        }
        
        
        $data=[
            'status'=>1,
            'starttime'=>$nowtime,
        ];
        
        $result = $model->upConversation($touid,$uid,$showid,$data);
        if(!$result){
            $rs['code']=1005;
			$rs['msg']='接听失败，请重试';
			return $rs;
        }
        
        $model->upAnswer($uid);
        /* 更新状态 */
        $model->upOnline($uid,2);
        $model->upOnline($touid,2);
        
        /* 流地址 */
        $stream=$uid.'_'.$showid;
        $push=PrivateKey_tx('rtmp',$stream,1);
        $pull=tx_acc('rtmp',$stream,0);
        /* $pull=PrivateKey_tx('http',$stream,0); */
        $info=[
            'push'=>$push,
            'pull'=>$pull,
        ];

        $rs['info'][0]=$info;
        
        return $rs;
    }

    /* 用户接听 */
    public function userAnswer($uid,$liveuid,$showid){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$model = new Model_Live();
        
        $conversainfo = $model->getConversation($uid,$liveuid,$showid);
        if(!$conversainfo){
            $rs['code']=1002;
			$rs['msg']='信息错误';
			return $rs;
        }
        
        if($conversainfo['status']!=0){
            $rs['code']=1003;
			$rs['msg']='信息错误';
			if($conversainfo['status']==2){
				$rs['msg']='对方结束通话';
			}
			return $rs;
        }
        $nowtime=time();
        if($nowtime - $showid > 60 ){
            $rs['code']=1004;
			$rs['msg']='信息错误';
			return $rs;
        }
        
        
        $data=[
            'status'=>1,
            'starttime'=>$nowtime,
        ];
        
        $result = $model->upConversation($uid,$liveuid,$showid,$data);
        if(!$result){
            $rs['code']=1005;
			$rs['msg']='接听失败，请重试';
			return $rs;
        }
        
        //$model->upAnswer($uid);
        /* 更新状态 */
        $model->upOnline($uid,2);
        $model->upOnline($liveuid,2);
        
        $coin=$conversainfo['coin'];
        $type=$conversainfo['type'];
        /* 流地址 */
        $stream=$uid.'_'.$showid;
        $push=PrivateKey_tx('rtmp',$stream,1);
        $pull=tx_acc('rtmp',$stream,0);
        /* $pull=PrivateKey_tx('http',$stream,0);  */
        $info=[
            'total'=>$coin,
            'type'=>$type,
            'push'=>$push,
            'pull'=>$pull,
        ];

        $rs['info'][0]=$info;
        
        return $rs;
    }
    
    /**
     * 挂断
     * @param  int $type 0 用户挂断 1 主播挂断
     */
    public function hangUp($uid,$liveuid,$showid,$hangtype,$type){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$model = new Model_Live();
        
        $conversainfo = $model->getConversation($uid,$liveuid,$showid);

	
        if(!$conversainfo){
            $rs['code']=1002;
			$rs['msg']='信息错误';
			return $rs;
        }

        if($conversainfo['status'] !=2 ){
            /* 通话中时挂断 */
            /* 更新状态 */
            $model->upOnline($uid,3);
            $model->upOnline($liveuid,3);
        }
        
        if($conversainfo['status']==2){


            if($type==0){
                return $rs;
            }

            if($type==1){

                /* 主播挂断 */
                $info=[
                    'length'=>'0',
                    'gifttotal'=>'0',
                    'answertotal'=>'0',
                ];
                if($conversainfo['endtime'] && $conversainfo['starttime']){
                    /* 通话中挂断 */
                    $cha=$conversainfo['endtime'] - $conversainfo['starttime'];
                    
					$length=getLength($cha);
					

                    $info=[
                        'length'=>$length,
                        'gifttotal'=>$conversainfo['gift'],
                        'answertotal'=>$conversainfo['total'],
                    ];
					

                }
                
                $rs['info'][0]=$info;
                return $rs;
            }          
        }
        
        $nowtime=time();


		
        $data=[
            'status'=>2,
            'endtime'=>$nowtime,
            'time'=>date("Y-m-d",$nowtime)
        ];
		
		 
        /* 10秒内关闭 扣除通话收益*/
        if($type==1){
            if($nowtime - $conversainfo['starttime'] < 10){
                $votes=$conversainfo['total'];
                $undivide_total=$conversainfo['undivide_total'];
                $conversainfo['total']='0';
                reduceVotes($liveuid,$votes,$undivide_total);

                //增加映票记录
                $vote_record=array(
                    'type'=>0,
                    'action'=>8, //通话不足10秒退回主播收益给平台
                    'uid'=>$liveuid,
                    'fromid'=>$uid,
                    'actionid'=>0,
                    'nums'=>0,
                    'total'=>$votes,
                    'showid'=>$showid,
                    'votes'=>$votes,
                    'addtime'=>$nowtime
                );

                addVoteRecord($vote_record);
                
                $model->reduceRecord($liveuid,$showid);
                
                $data['total']='0';

                //扣除主播所属家族的家族长映票
                $family_uid=getUserFamilyer($liveuid);

                if($family_uid){
                    deductFamilyerVote($family_uid,$liveuid,$showid);
                }

            }
        }
        
        $result = $model->upConversation($uid,$liveuid,$showid,$data);

        if(!$result){
            $rs['code']=1005;
			$rs['msg']='挂断失败，请重试';
			return $rs;
        }
        
        if($type==0){
            /* 用户挂断 */
            if($conversainfo['status']==0){
                /* 等待时挂断 */
                if($hangtype==0){
                    $model->upTotal($liveuid,1);
                }
            }
        }
        
        
        if($type==1){
            /* 主播挂断 */
            $info=[
                'length'=>'0',
                'gifttotal'=>'0',
                'answertotal'=>'0',
            ];
            if($conversainfo['status']==1){
                /* 通话中挂断 */
                $cha=$nowtime - $conversainfo['starttime'];
				$length=getLength($cha);
                $info=[
                    'length'=>$length,
                    'gifttotal'=>$conversainfo['gift'],
                    'answertotal'=>$conversainfo['total'],
                ];
	
            }
            
            $rs['info'][0]=$info;
        }
        
        return $rs;
    }

    /* 房间扣费 */
	public function roomCharge($uid,$liveuid,$showid) {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());

		$model = new Model_Live();
        
        $conversainfo = $model->getConversation($uid,$liveuid,$showid);

        if(!$conversainfo){
            $rs['code']=1002;
			$rs['msg']='信息错误';
			return $rs;
        }
        
        if($conversainfo['status']!=1){
            $rs['code']=1003;
			$rs['msg']='信息错误';
			return $rs;
        }
        
        
        $total= $conversainfo['coin'];
		 
		$addtime=time();
		$type='0';
		$action='2';
        $vote_action='4'; //视频通话收费
        if($conversainfo['type']==2){ //语音
            $action='3';
            $vote_action='5'; //语音通话收费
        }
		
		/* 更新用户余额 消费 */
		$ifok =upCoin($uid,$total);
        if(!$ifok){
            $rs['code'] = 1003;
			$rs['msg'] = '余额不足';
			return $rs;
        }

        $overage=0;

		/* 更新直播 魅力值 累计魅力值 */
		addVotes($liveuid,$overage,$total);

		$insert=array(
            "type"=>$type,
            "action"=>$action,
            "uid"=>$uid,
            "touid"=>$liveuid,
            "actionid"=>0,
            "nums"=>1,
            "totalcoin"=>$total,
            "showid"=>$showid,
            "addtime"=>$addtime 
        );
        
        mergeCoinRecord($insert);

        //添加映票收入记录
        $data=array(
            'type'=>1,
            'action'=>$vote_action,
            'uid'=>$liveuid,
            'fromid'=>$uid,
            'actionid'=>0,
            'nums'=>1,
            'total'=>$overage,
            'showid'=>$showid,
            'votes'=>$overage,
            'addtime'=>$addtime

        );

        mergeVoteRecord($data);
        
        //更新通话收益
        $model->upConTotal($uid,$liveuid,$showid,$overage,$total);

		$userinfo =getUserCoin($uid);
			 
		$level=getLevel($userinfo['consumption']);	
		
		$info=array(
            "level"=>$level,
            "coin"=>$userinfo['coin'],
            "istips"=>'0',
            "tips"=>'',
        );
        
        if($userinfo['coin'] < $total * 1){
            $info['istips']='1';
            $info['tips']='您当前账户余额已不足与主播通话1分钟请及时进行充值';
            
        }else if($userinfo['coin'] < $total * 5 && $userinfo['coin'] >= $total * 4){
            $info['istips']='1';
            $info['tips']='您当前账户余额已不足与主播通话5分钟请及时进行充值';
        }else if($userinfo['coin'] < $total * 10  && $userinfo['coin'] >= $total * 9){
            $info['istips']='1';
            $info['tips']='您当前账户余额已不足与主播通话10分钟请及时进行充值';
        }
		
        $rs['info'][0]=$info;
		return $rs; 
	}

    public function checkConversa($showid){
        $model = new Model_Live();
        $result=$model->getConversationByShowid($showid);
        return $result;
    }

	
}
