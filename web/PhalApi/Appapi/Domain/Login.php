<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-03-22
// +—————————————————————————————————————————————————————————————————————


class Domain_Login {

    public function userLogin($user_login,$user_pass,$source,$country_code) {
        $rs = array();
        
        $configpri=getConfigPri();

        $where=[
            'user_login = ?'=>$user_login
        ];
        $model = new Model_Login();
        $info = $model->userLogin($where);
        
        if(!$info){
			
			//注册奖励
			$configpri=getConfigPri();
			$reg_reward=$configpri['reg_reward'];
			
            /* 注册 */
            $nickname='手机用户'.substr($user_login,-3);
			$data=array(
                'country_code'=>$country_code,
				'user_login' => $user_login,
				'user_nickname' =>$nickname,
				"source"=>$source,
				"mobile"=>$user_login,
				"coin"=>$reg_reward,
			);
            
            $info = $model->userReg($data);

            if($reg_reward){
            	//添加钻石记录
	            $insert=array(
	                'type'=>1,
	                'action'=>8, //注册奖励
	                'uid'=>$info['id'],
	                'touid'=>$info['id'],
	                'actionid'=>0,
	                'nums'=>0,
	                'totalcoin'=>$reg_reward,
	                'addtime'=>time()
	            );

	            addCoinRecord($insert);
            }
  

        }
        
        
		if($info['user_status']=='0'){
			return 1002;
        }
		if($info['user_status']=='3'){
			return 1003;
        }
		unset($info['user_status']);
		
		$info['isreg']='0';
		$info['issexrecommend']='0';
        
		if($info['sex']==0){
			$info['isreg']='1';
			$info['issexrecommend']='1';
		}
        
        unset($info['last_login_time']);
 
		$info['level']=getLevel($info['consumption']);
		$info['level_anchor']=getLevelanchor($info['votestotal']);

		$info['avatar']=get_upload_path($info['avatar']);
		$info['avatar_thumb']=get_upload_path($info['avatar_thumb']);

        $userinfo=$info;
		unset($userinfo['issexrecommend']);
        unset($userinfo['isreg']);
        unset($userinfo['user_status']);
        unset($userinfo['coin']);
        unset($userinfo['login_type']);
        
        hMSet('userinfo_'.$userinfo['id'],$userinfo);
        
        
        $token=md5(md5($info['id'].$user_login.time()));
		$info['token']=$token;
		$model->updateToken($info['id'],$token);
        
        $usersig=setSig($info['id']);
		$info['usersig']=$usersig;
        
        unset($info['goodnums']);
        unset($info['badnums']);
        
        return $info;

    }
	
	public function setUserOnline($uid,$online){
        $rs = array();

        $model = new Model_Live();
      
        /* 更新状态 */
        $model->upOnline($uid,$online);

        return $rs;
    }


}
